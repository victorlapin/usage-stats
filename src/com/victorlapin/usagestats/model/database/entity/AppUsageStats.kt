package com.victorlapin.usagestats.model.database.entity

import android.graphics.drawable.Drawable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "app_stats")
data class AppUsageStats(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    @ColumnInfo(name = "package_name")
    val packageName: String,
    @ColumnInfo(name = "usage")
    var usage: Long,
    @ColumnInfo(name = "launch_count")
    var launchCount: Int = 0,
    @ColumnInfo(name = "stats_date")
    val statsDate: Long
) {
    @Ignore
    var appName: String? = null
    @Ignore
    var image: Drawable? = null
    @Ignore
    var isDeleted = false
}