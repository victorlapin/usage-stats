package com.victorlapin.usagestats.presenter

import com.victorlapin.usagestats.model.AboutClickEventArgs
import com.victorlapin.usagestats.model.interactor.AboutInteractor
import com.victorlapin.usagestats.view.AboutFragmentView
import moxy.InjectViewState
import moxy.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class AboutFragmentPresenter(
    private val mInteractor: AboutInteractor,
    private val mRouter: Router
) : MvpPresenter<AboutFragmentView>() {
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        showData()
    }

    private fun showData() =
        viewState.setData(mInteractor.getData())

    fun onItemClick(args: AboutClickEventArgs) =
        mRouter.navigateTo(args.screen)

    fun onBackPressed() = mRouter.exit()
}