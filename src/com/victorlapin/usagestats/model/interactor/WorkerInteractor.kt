package com.victorlapin.usagestats.model.interactor

import com.victorlapin.usagestats.model.DateBuilder
import com.victorlapin.usagestats.model.repository.WorkerRepository
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.concurrent.TimeUnit

class WorkerInteractor(
    private val mRepo: WorkerRepository
) {
    fun scheduleAppUsageWorker(): Completable {
        val runTime = DateBuilder().dateTo + TimeUnit.MINUTES.toMillis(1)
        return mRepo.scheduleAppUsageWorker(runTime)
    }

    fun checkAppUsageWorkerScheduled(): Single<Boolean> =
        mRepo.checkAppUsageWorkerScheduled()
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())

    fun schedulePeriodicWorker(): Completable = mRepo.schedulePeriodicWorker()

    fun checkPeriodicWorkerScheduled(): Single<Boolean> =
        mRepo.checkPeriodicWorkerScheduled()
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())

    fun scheduleDatabaseHygieneWorker(): Completable = mRepo.scheduleDatabaseHygieneWorker()

    fun checkDatabaseHygieneWorkerScheduled(): Single<Boolean> =
        mRepo.checkDatabaseHygieneWorkerScheduled()
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())

    fun cancelAllWorkers(): Completable = mRepo.cancelPeriodicWorker()
        .andThen(mRepo.cancelAppUsageWorker())
        .andThen(mRepo.cancelDatabaseHygieneWorker())
        .subscribeOn(Schedulers.computation())
        .observeOn(AndroidSchedulers.mainThread())
}