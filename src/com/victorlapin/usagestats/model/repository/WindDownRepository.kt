package com.victorlapin.usagestats.model.repository

import android.app.AlarmManager
import android.app.NotificationManager
import com.victorlapin.usagestats.manager.ServicesManager
import com.victorlapin.usagestats.manager.SettingsManager
import io.reactivex.Completable
import timber.log.Timber
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class WindDownRepository(
    private val mSettings: SettingsManager,
    private val mServices: ServicesManager
) {
    fun scheduleWindDown(startTime: Long, endTime: Long): Completable = Completable.create { emitter ->
        Timber.tag(mTag).i(
            "Next run start: ${SimpleDateFormat.getDateTimeInstance(
                DateFormat.MEDIUM,
                DateFormat.SHORT
            ).format(Date(startTime))}"
        )
        mServices.alarmManager.setRepeating(
            AlarmManager.RTC_WAKEUP, startTime,
            TimeUnit.DAYS.toMillis(1), mServices.windDownStartIntent
        )
        Timber.tag(mTag).i(
            "Next run end: ${SimpleDateFormat.getDateTimeInstance(
                DateFormat.MEDIUM,
                DateFormat.SHORT
            ).format(Date(endTime))}"
        )
        mServices.alarmManager.setRepeating(
            AlarmManager.RTC_WAKEUP, endTime,
            TimeUnit.DAYS.toMillis(1), mServices.windDownEndIntent
        )
        mServices.enableWindDownBootReceiver()
        emitter.onComplete()
    }

    fun cancelWindDown(): Completable = Completable.create { emitter ->
        Timber.tag(mTag).i("Canceled")
        mServices.alarmManager.cancel(mServices.windDownStartIntent)
        mServices.alarmManager.cancel(mServices.windDownEndIntent)
        mServices.disableWindDownBootReceiver()
        emitter.onComplete()
    }

    fun turnOn() {
        if (
            mSettings.isWindDownGrayscale &&
            mServices.isSecureSettingsGranted() &&
            !mServices.isGrayscaled()
        ) {
            mServices.setGrayscale(true)
        }

        if (
            mSettings.isWindDownDnd &&
            mServices.isNotificationPolicyAccessGranted()
        ) {
            mServices.setDnd(mSettings.windDownDndMode)
        }
        mSettings.isWindDownNow = true
    }

    fun turnOff() {
        if (
            mServices.isSecureSettingsGranted() &&
            mServices.isGrayscaled()
        ) {
            mServices.setGrayscale(false)
        }

        if (
            mServices.isNotificationPolicyAccessGranted()
        ) {
            mServices.setDnd(NotificationManager.INTERRUPTION_FILTER_ALL)
        }
        mSettings.isWindDownNow = false
    }

    fun isWindDownNow() = mSettings.isWindDownNow

    fun scheduleExitFromPendingWindDown(exitTime: Long) {
        mServices.alarmManager.setExactAndAllowWhileIdle(
            AlarmManager.RTC_WAKEUP, exitTime,
            mServices.windDownPendingEndIntent
        )
        Timber.tag(mTag).i(
            "Pending Wind Down will stop at: ${SimpleDateFormat.getDateTimeInstance(
                DateFormat.MEDIUM,
                DateFormat.SHORT
            ).format(Date(exitTime))}"
        )
    }

    companion object {
        private const val mTag = "WindDownRepository"
    }
}