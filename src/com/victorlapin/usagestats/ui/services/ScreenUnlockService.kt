package com.victorlapin.usagestats.ui.services

import android.app.Notification
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.IBinder
import androidx.core.app.NotificationCompat
import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.manager.ResourcesManager
import com.victorlapin.usagestats.presenter.ScreenUnlockServicePresenter
import org.koin.android.ext.android.getKoin
import org.koin.core.qualifier.named

class ScreenUnlockService : Service() {
    private val mScope = getKoin().createScope(
        scopeId = "ScreenUnlockService",
        qualifier = named<ScreenUnlockService>()
    )
    private val mResources by mScope.inject<ResourcesManager>()
    private val mPresenter by mScope.inject<ScreenUnlockServicePresenter>()

    private val mIntentFilter = IntentFilter(Intent.ACTION_USER_PRESENT)
    private val mReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.action == Intent.ACTION_USER_PRESENT) {
                mPresenter.insertScreenUnlock()
            }
        }
    }

    override fun onBind(intent: Intent?): IBinder? = null

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.action ?: ACTION_START) {
            ACTION_START -> {
                startForeground(FOREGROUND_ID, getNotification())
            }
            ACTION_STOP -> {
                stopForeground(true)
                stopSelf()
            }
        }
        return START_STICKY
    }

    override fun onCreate() {
        super.onCreate()
        registerReceiver(mReceiver, mIntentFilter)
    }

    override fun onDestroy() {
        mScope.close()
        unregisterReceiver(mReceiver)
        super.onDestroy()
    }

    private fun getNotification(): Notification =
        NotificationCompat.Builder(
            this,
            mResources.getString(R.string.channel_default_id)
        )
            .setSmallIcon(R.drawable.cellphone_android)
            .setContentTitle(mResources.getString(R.string.service_screen_unlock))
            .setContentText(mResources.getString(R.string.service_screen_unlock_content))
            .build()

    companion object {
        private const val FOREGROUND_ID = 200
        const val ACTION_START = "ACTION_START"
        const val ACTION_STOP = "ACTION_STOP"
    }
}