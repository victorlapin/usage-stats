package com.victorlapin.usagestats.presenter

import com.victorlapin.usagestats.model.interactor.CollectedEventsInteractor

class ScreenUnlockServicePresenter(
    private val mEventsInteractor: CollectedEventsInteractor
) {
    fun insertScreenUnlock() = mEventsInteractor.insertScreenUnlock()
}