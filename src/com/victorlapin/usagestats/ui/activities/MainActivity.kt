package com.victorlapin.usagestats.ui.activities

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.presenter.MainActivityPresenter
import com.victorlapin.usagestats.ui.fragments.HomeFragment
import com.victorlapin.usagestats.view.MainActivityView
import moxy.ktx.moxyPresenter
import org.koin.androidx.scope.currentScope
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command

class MainActivity : BaseActivity(), MainActivityView {
    override val layoutRes = R.layout.activity_generic_no_toolbar

    private val presenter by moxyPresenter { currentScope.get<MainActivityPresenter>() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if (savedInstanceState == null) {
            if (intent.hasExtra(EXTRA_SCREEN_KEY)) {
                presenter.showFragment(intent.getStringExtra(EXTRA_SCREEN_KEY))
            } else {
                presenter.showFragment(null)
            }
        }
    }

    override val navigator = object : SupportAppNavigator(this, R.id.fragment_container) {
        override fun setupFragmentTransaction(
            command: Command?,
            currentFragment: Fragment?,
            nextFragment: Fragment?,
            fragmentTransaction: FragmentTransaction?
        ) {
            nextFragment?.let { fragment ->
                fragmentTransaction?.let {
                    if (fragment !is HomeFragment) {
                        it.setCustomAnimations(
                            R.animator.slide_up, R.animator.fade_out,
                            R.animator.fade_in, R.animator.slide_down
                        )
                    }
                }
            }
        }
    }

    companion object {
        const val EXTRA_SCREEN_KEY = "EXTRA_SCREEN_KEY"
    }
}