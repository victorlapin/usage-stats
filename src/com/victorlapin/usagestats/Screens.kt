package com.victorlapin.usagestats

import android.annotation.SuppressLint
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.provider.Settings
import androidx.fragment.app.Fragment
import com.google.android.gms.oss.licenses.OssLicensesMenuActivity
import com.victorlapin.usagestats.ui.fragments.AboutFragment
import com.victorlapin.usagestats.ui.fragments.AppsFragment
import com.victorlapin.usagestats.ui.fragments.HomeFragment
import com.victorlapin.usagestats.ui.fragments.NetworkFragment
import com.victorlapin.usagestats.ui.fragments.NotificationsFragment
import com.victorlapin.usagestats.ui.fragments.SettingsGlobalFragment
import com.victorlapin.usagestats.ui.fragments.SettingsServicesFragment
import com.victorlapin.usagestats.ui.fragments.SettingsWindDownFragment
import com.victorlapin.usagestats.ui.fragments.UnlocksFragment
import ru.terrakok.cicerone.android.support.SupportAppScreen

class HomeScreen : SupportAppScreen() {
    override fun getFragment(): Fragment = HomeFragment.newInstance()
}

class SettingsScreen : SupportAppScreen() {
    override fun getFragment(): Fragment = SettingsGlobalFragment.newInstance()
}

class SettingsServicesScreen : SupportAppScreen() {
    override fun getFragment(): Fragment = SettingsServicesFragment.newInstance()
}

class NotificationsScreen(private val dateOffset: Int) : SupportAppScreen() {
    override fun getFragment(): Fragment = NotificationsFragment.newInstance(dateOffset)
}

class UnlocksScreen(private val dateOffset: Int) : SupportAppScreen() {
    override fun getFragment(): Fragment = UnlocksFragment.newInstance(dateOffset)
}

class AppsScreen(private val dateOffset: Int) : SupportAppScreen() {
    override fun getFragment(): Fragment = AppsFragment.newInstance(dateOffset)
}

class AboutScreen : SupportAppScreen() {
    override fun getFragment(): Fragment = AboutFragment.newInstance()
}

class UsageAccessScreen : SupportAppScreen() {
    override fun getActivityIntent(context: Context?): Intent =
        Intent(Settings.ACTION_USAGE_ACCESS_SETTINGS)
}

class NotificationAccessScreen : SupportAppScreen() {
    override fun getActivityIntent(context: Context?): Intent =
        Intent(Settings.ACTION_NOTIFICATION_LISTENER_SETTINGS)
}

class AboutExternalScreen(private val url: String) : SupportAppScreen() {
    override fun getActivityIntent(context: Context?): Intent =
        Intent(Intent.ACTION_VIEW, Uri.parse(url))
}

class OssScreen : SupportAppScreen() {
    override fun getActivityIntent(context: Context): Intent {
        OssLicensesMenuActivity.setActivityTitle(context.getString(R.string.about_links_oss))
        return Intent(context, OssLicensesMenuActivity::class.java)
    }
}

class AppDetailsScreen(private val packageName: String) : SupportAppScreen() {
    override fun getActivityIntent(context: Context?): Intent =
        Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).apply {
            data = Uri.parse("package:$packageName")
        }
}

class SystemDndScreen : SupportAppScreen() {
    @SuppressLint("InlinedApi")
    override fun getActivityIntent(context: Context?): Intent =
        Intent(Settings.ACTION_ZEN_MODE_PRIORITY_SETTINGS).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        }
}

class SettingsWindDownScreen : SupportAppScreen() {
    override fun getFragment(): Fragment = SettingsWindDownFragment.newInstance()
}

class SystemNotificationsScreen : SupportAppScreen() {
    override fun getActivityIntent(context: Context?): Intent {
        val name = ComponentName(
            "com.android.settings",
            "com.android.settings.Settings\$NotificationAppListActivity"
        )
        return Intent().apply {
            component = name
        }
    }
}

class NetworkScreen(private val dateOffset: Int) : SupportAppScreen() {
    override fun getFragment(): Fragment = NetworkFragment.newInstance(dateOffset)
}