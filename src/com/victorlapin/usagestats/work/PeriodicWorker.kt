package com.victorlapin.usagestats.work

import android.content.Context
import androidx.work.PeriodicWorkRequest
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import com.victorlapin.usagestats.model.interactor.WorkerInteractor
import io.reactivex.Completable
import io.reactivex.Single
import org.koin.core.KoinComponent
import org.koin.core.inject
import timber.log.Timber
import java.util.concurrent.TimeUnit

class PeriodicWorker(context: Context, params: WorkerParameters) :
    RxWorker(context, params), KoinComponent {
    private val mWorkerInteractor by inject<WorkerInteractor>()

    override fun createWork(): Single<Result> = mWorkerInteractor
        .checkAppUsageWorkerScheduled()
        .flatMapCompletable {
            if (it) {
                Timber.tag(WORK_TAG).i("App usage worker already scheduled")
                Completable.complete()
            } else {
                mWorkerInteractor.scheduleAppUsageWorker()
            }
        }
        .toSingleDefault(Result.success())
        .onErrorReturnItem(Result.retry())
        .doOnSubscribe {
            Timber.tag(WORK_TAG).i("Worker started")
        }
        .doOnError { Timber.tag(WORK_TAG).e(it) }
        .doFinally {
            Timber.tag(WORK_TAG).i("Worker finished")
        }

    companion object {
        const val WORK_TAG = "PeriodicWorker"

        fun buildRequest(): PeriodicWorkRequest {
            return PeriodicWorkRequest.Builder(
                PeriodicWorker::class.java,
                1L, TimeUnit.HOURS
            )
                .build()
        }
    }
}