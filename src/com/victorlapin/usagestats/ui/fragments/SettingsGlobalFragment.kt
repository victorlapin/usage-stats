package com.victorlapin.usagestats.ui.fragments

import android.os.Bundle
import android.text.InputType
import android.text.method.DigitsKeyListener
import android.widget.TextView
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import com.afollestad.assent.Permission
import com.afollestad.assent.askForPermissions
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.customview.customView
import com.afollestad.materialdialogs.customview.getCustomView
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import com.victorlapin.usagestats.AboutScreen
import com.victorlapin.usagestats.Const
import com.victorlapin.usagestats.FileTree
import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.SettingsServicesScreen
import com.victorlapin.usagestats.di.PROP_DAYS_COUNT_CHANGED
import com.victorlapin.usagestats.di.PROP_STATS_MODE_CHANGED
import com.victorlapin.usagestats.manager.LogManager
import com.victorlapin.usagestats.manager.ResourcesManager
import com.victorlapin.usagestats.manager.SettingsManager
import com.victorlapin.usagestats.model.interactor.AppUsageInteractor
import com.victorlapin.usagestats.model.interactor.CollectedEventsInteractor
import com.victorlapin.usagestats.model.interactor.WindDownInteractor
import com.victorlapin.usagestats.model.interactor.WorkerInteractor
import com.victorlapin.usagestats.ui.activities.BaseActivity
import kotlinx.android.synthetic.main.dialog_input.view.*
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class SettingsGlobalFragment : PreferenceFragmentCompat() {
    private val mSettings by inject<SettingsManager>()
    private val mResources by inject<ResourcesManager>()
    private val mLogs by inject<LogManager>()
    private val mEventsInteractor by inject<CollectedEventsInteractor>()
    private val mAppsInteractor by inject<AppUsageInteractor>()
    private val mWorkerInteractor by inject<WorkerInteractor>()
    private val mWindDownInteractor by inject<WindDownInteractor>()
    private val mRouter by inject<Router>()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences_global)

        val themePreference = findPreference<Preference>(SettingsManager.KEY_THEME)
        val entries = listOf(
            mResources.getString(R.string.theme_light),
            mResources.getString(R.string.theme_dark),
            mResources.getString(R.string.theme_black)
        )
        val values = listOf(
            Integer.toString(R.style.AppTheme_Light_Pixel),
            Integer.toString(R.style.AppTheme_Dark_Pixel),
            Integer.toString(R.style.AppTheme_Black_Pixel)
        )
        val initialSelection = values.indexOf(mSettings.theme.toString())

        themePreference!!.setOnPreferenceClickListener {
            MaterialDialog(context!!).show {
                lifecycleOwner(this@SettingsGlobalFragment)
                title(R.string.pref_title_theme)
                listItemsSingleChoice(
                    items = entries,
                    initialSelection = initialSelection,
                    waitForPositiveButton = false
                ) { dialog, index, _ ->
                    dialog.dismiss()
                    val theme = Integer.valueOf(values[index])
                    mSettings.theme = theme
                    (activity as BaseActivity).updateTheme(theme)
                }
                negativeButton(android.R.string.cancel)
            }
            true
        }

        findPreference<Preference>(SettingsManager.KEY_ABOUT)!!.setOnPreferenceClickListener {
            mRouter.navigateTo(AboutScreen())
            return@setOnPreferenceClickListener true
        }

        findPreference<Preference>(SettingsManager.KEY_CLEAR_DATABASE)!!.setOnPreferenceClickListener {
            MaterialDialog(context!!).show {
                lifecycleOwner(this@SettingsGlobalFragment)
                title(R.string.pref_clear_database_title)
                message(R.string.confirmation)
                negativeButton(android.R.string.no)
                positiveButton(android.R.string.yes) {
                    mEventsInteractor.clear()
                    mAppsInteractor.clear()
                }
            }
            return@setOnPreferenceClickListener true
        }

        findPreference<Preference>(SettingsManager.KEY_RESET_FIRST_RUN)!!.setOnPreferenceClickListener {
            mSettings.isFirstRun = true
            mSettings.needsTapTarget = true
            return@setOnPreferenceClickListener true
        }

        findPreference<Preference>(Const.FRAGMENT_SETTINGS_SERVICES)!!.setOnPreferenceClickListener {
            mRouter.navigateTo(SettingsServicesScreen())
            return@setOnPreferenceClickListener true
        }

        val logPreference = findPreference<Preference>(SettingsManager.KEY_ENABLE_FILE_LOG)
        logPreference!!.summary = mResources.getString(R.string.pref_enable_file_log_summary)
            .format(FileTree.LOG_FILE.absolutePath)
        logPreference.setOnPreferenceChangeListener { it, newValue ->
            val b = newValue as Boolean
            if (b) {
                askForPermissions(Permission.WRITE_EXTERNAL_STORAGE) { result ->
                    if (result.isAllGranted(Permission.WRITE_EXTERNAL_STORAGE)) {
                        mLogs.enableFileLogs()
                    } else {
                        mLogs.disableFileLogs()
                        mSettings.enableFileLog = false
                        (it as SwitchPreference).isChecked = false
                    }
                }
            } else {
                mLogs.disableFileLogs()
            }
            true
        }

        findPreference<Preference>(SettingsManager.KEY_CANCEL_WORKERS)!!.setOnPreferenceClickListener {
            mWorkerInteractor.cancelAllWorkers().subscribe()
            return@setOnPreferenceClickListener true
        }

        val daysToKeepPreference = findPreference<Preference>(SettingsManager.KEY_DAYS_TO_KEEP)
        daysToKeepPreference!!.summary = mResources.getString(
            R.string.pref_days_to_keep_summary,
            mSettings.daysToKeep
        )
        daysToKeepPreference.setOnPreferenceClickListener { pref ->
            val dialog = MaterialDialog(context!!)
                .lifecycleOwner(this@SettingsGlobalFragment)
                .title(R.string.pref_days_to_keep_title)
                .customView(viewRes = R.layout.dialog_input, scrollable = true)
                .negativeButton(android.R.string.cancel)
                .positiveButton(android.R.string.ok) { dialog ->
                    try {
                        val view = dialog.getCustomView()
                        val i = view.edt_input.text.toString().toInt()
                        if (i > 0) {
                            mSettings.daysToKeep = i
                            getKoin().setProperty(PROP_DAYS_COUNT_CHANGED, true)
                            pref.summary = mResources.getString(
                                R.string.pref_days_to_keep_summary,
                                i
                            )
                        }
                    } catch (ignore: Exception) {
                    }
                }

            dialog.getCustomView().apply {
                this.edt_input.setText(
                    mSettings.daysToKeep.toString(),
                    TextView.BufferType.EDITABLE
                )
                this.edt_input.inputType = InputType.TYPE_CLASS_NUMBER
                this.edt_input.keyListener = DigitsKeyListener.getInstance("0123456789")
            }
            dialog.show()
            true
        }

        val fullStatsPreference = findPreference<Preference>(SettingsManager.KEY_SHOW_FULL_STATS)
        fullStatsPreference!!.setOnPreferenceChangeListener { _, _ ->
            getKoin().setProperty(PROP_STATS_MODE_CHANGED, true)
            true
        }

        findPreference<Preference>(SettingsManager.KEY_WIND_DOWN_FORCE_STOP)!!.setOnPreferenceClickListener {
            mWindDownInteractor.turnOff()
            true
        }
    }

    companion object {
        fun newInstance() = SettingsGlobalFragment()
    }
}