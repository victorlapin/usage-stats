package com.victorlapin.usagestats.model.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.RawQuery
import androidx.room.Transaction
import androidx.sqlite.db.SimpleSQLiteQuery
import androidx.sqlite.db.SupportSQLiteQuery
import com.victorlapin.usagestats.model.AggregatedEvent
import com.victorlapin.usagestats.model.database.entity.CollectedEvent
import io.reactivex.Single

@Dao
interface CollectedEventDao {
    @Query("select count(*) from collected_events where event_type = :eventType and event_dt >= :dateFrom and event_dt <= :dateTo")
    fun getEventsCount(eventType: Int, dateFrom: Long, dateTo: Long): Int

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(event: CollectedEvent)

    @Transaction
    @Query("delete from collected_events")
    fun clear()

    @Query("select package_name as appName, count(*) as quantity from collected_events where event_type = :eventType and event_dt >= :dateFrom and event_dt <= :dateTo group by package_name order by count(*) desc")
    fun getAggregatedEvents(
        eventType: Int,
        dateFrom: Long,
        dateTo: Long
    ): Single<List<AggregatedEvent>>

    @Query("select * from collected_events where event_type = :eventType and event_dt >= :dateFrom and event_dt <= :dateTo")
    fun getEvents(eventType: Int, dateFrom: Long, dateTo: Long): Single<List<CollectedEvent>>

    @Transaction
    @Query("delete from collected_events where event_dt < :date")
    fun deleteOldEvents(date: Long): Int

    @RawQuery
    fun vacuum(query: SupportSQLiteQuery = SimpleSQLiteQuery("vacuum")): Int
}