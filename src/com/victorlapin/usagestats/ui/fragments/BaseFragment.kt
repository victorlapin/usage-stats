package com.victorlapin.usagestats.ui.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleObserver
import moxy.MvpAppCompatFragment
import org.koin.androidx.scope.bindScope
import org.koin.androidx.scope.currentScope

abstract class BaseFragment : MvpAppCompatFragment(), LifecycleObserver {
    abstract val layoutRes: Int

    override fun onCreate(savedInstanceState: Bundle?) {
        bindScope(currentScope)
        super.onCreate(savedInstanceState)
        lifecycle.addObserver(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? =
        inflater.inflate(layoutRes, container, false)
}