package com.victorlapin.usagestats.ui

import io.reactivex.subjects.PublishSubject

class TodayEventBridge {
    private val mTodaySubject: PublishSubject<Any> = PublishSubject.create()

    val todayClickedEvent: PublishSubject<Any> = mTodaySubject
}