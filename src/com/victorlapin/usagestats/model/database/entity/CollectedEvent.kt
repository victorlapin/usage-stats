package com.victorlapin.usagestats.model.database.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "collected_events")
data class CollectedEvent(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    @ColumnInfo(name = "event_type")
    val eventType: Int,
    @ColumnInfo(name = "event_dt")
    val eventDT: Long,
    @ColumnInfo(name = "package_name")
    val packageName: String? = null
) {
    companion object {
        const val TYPE_SCREEN_UNLOCK = 1
        const val TYPE_NOTIFICATION = 2
    }
}