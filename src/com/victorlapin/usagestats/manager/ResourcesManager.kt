package com.victorlapin.usagestats.manager

import android.content.Context
import android.content.res.Resources
import android.graphics.drawable.Drawable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.RelativeSizeSpan
import androidx.annotation.ArrayRes
import androidx.annotation.ColorRes
import androidx.annotation.DrawableRes
import androidx.annotation.PluralsRes
import androidx.annotation.StringRes
import androidx.core.content.ContextCompat
import com.victorlapin.usagestats.R

class ResourcesManager(private val mContext: Context) {
    fun getString(@StringRes id: Int, arg: Any? = null): String = if (arg != null)
        mContext.getString(id, arg) else mContext.getString(id)

    fun getQuantityString(@PluralsRes id: Int, quantity: Int, formatArg: Int): String =
        mContext.resources.getQuantityString(id, quantity, formatArg)

    fun getDrawable(@DrawableRes id: Int): Drawable? = mContext.getDrawable(id)

    fun getColor(@ColorRes id: Int): Int =
        ContextCompat.getColor(mContext, id)

    fun getStringArray(@ArrayRes id: Int): Array<String> =
        mContext.resources.getStringArray(id)

    fun getStringList(@ArrayRes id: Int): List<String> =
        getStringArray(id).toList()

    val resources: Resources
        get() = mContext.resources

    fun formatTraffic(bytes: Long): String {
        val kb = 1024L
        val mb = kb * 1024L
        val gb = mb * 1024L

        return when {
            bytes >= gb -> mContext.getString(R.string.traffic_gigabytes, bytes.toFloat() / gb.toFloat())
            bytes >= mb -> mContext.getString(R.string.traffic_megabytes, bytes.toFloat() / mb.toFloat())
            bytes >= kb -> mContext.getString(R.string.traffic_kilobytes, bytes.toFloat() / kb.toFloat())
            else -> mContext.getString(R.string.traffic_bytes, bytes)
        }
    }

    fun formatTrafficSpannable(bytes: Long): SpannableStringBuilder {
        val spannable = SpannableStringBuilder(formatTraffic(bytes))
        val largeSpan = RelativeSizeSpan(1f)
        val smallSpan = RelativeSizeSpan(0.6f)

        spannable.setSpan(
            largeSpan,
            0,
            spannable.indexOf(" "),
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        spannable.setSpan(
            smallSpan,
            spannable.indexOf(" "),
            spannable.length,
            Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
        )

        return spannable
    }
}