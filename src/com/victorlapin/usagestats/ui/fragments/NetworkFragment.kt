package com.victorlapin.usagestats.ui.fragments

import android.os.Bundle
import androidx.interpolator.view.animation.FastOutLinearInInterpolator
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.transition.Fade
import androidx.transition.TransitionManager
import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.model.database.entity.NetworkUsageStats
import com.victorlapin.usagestats.presenter.NetworkFragmentPresenter
import com.victorlapin.usagestats.ui.adapters.NetworkAdapter
import com.victorlapin.usagestats.view.NetworkFragmentView
import com.victorlapin.usagestats.visible
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.include_progress.*
import kotlinx.android.synthetic.main.include_toolbar.*
import moxy.ktx.moxyPresenter
import org.koin.android.ext.android.get
import org.koin.androidx.scope.currentScope
import org.koin.core.parameter.parametersOf

class NetworkFragment : BaseFragment(), NetworkFragmentView {
    override val layoutRes = R.layout.fragment_list

    private val mDateOffset: Int by lazy {
        arguments!!.getInt(ARG_DATE_OFFSET)
    }

    private val presenter by moxyPresenter {
        currentScope.get<NetworkFragmentPresenter>(parameters = { parametersOf(mDateOffset) })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbar.setTitle(R.string.action_network)
        toolbar.setNavigationIcon(R.drawable.close)
        toolbar.setNavigationOnClickListener { presenter.onBackPressed() }

        list.apply {
            layoutManager = LinearLayoutManager(context)
            itemAnimator = DefaultItemAnimator()
            setHasFixedSize(true)
            adapter = NetworkAdapter(get())
        }
    }

    override fun setData(data: List<NetworkUsageStats>) {
        list.post { (list.adapter as NetworkAdapter).setData(data) }
    }

    override fun toggleProgress(isVisible: Boolean) {
        coordinator.post {
            val transition = Fade().setInterpolator(FastOutLinearInInterpolator())
            TransitionManager.beginDelayedTransition(coordinator, transition)
            progress_bar_layout.visible(isVisible)
        }
    }

    companion object {
        private const val ARG_DATE_OFFSET = "arg_date_offset"

        fun newInstance(dateOffset: Int): NetworkFragment {
            val args = Bundle().apply {
                putInt(ARG_DATE_OFFSET, dateOffset)
            }

            return NetworkFragment().apply {
                arguments = args
            }
        }
    }
}