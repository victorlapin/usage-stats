package com.victorlapin.usagestats.ui.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.victorlapin.usagestats.presenter.WindDownPresenter
import org.koin.core.KoinComponent
import org.koin.core.qualifier.named
import timber.log.Timber

class WindDownStartReceiver : BroadcastReceiver(), KoinComponent {
    private var mScope = getKoin().createScope(
        scopeId = "WindDownStartReceiver",
        qualifier = named<WindDownStartReceiver>()
    )
    private val mPresenter by mScope.inject<WindDownPresenter>()

    override fun onReceive(context: Context, intent: Intent) {
        Timber.i("Starting Wind Down...")
        mPresenter.startWindDown()
        mScope.close()
    }
}