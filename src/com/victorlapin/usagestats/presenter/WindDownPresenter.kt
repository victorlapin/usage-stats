package com.victorlapin.usagestats.presenter

import com.victorlapin.usagestats.model.interactor.WindDownInteractor

class WindDownPresenter(
    private val mInteractor: WindDownInteractor
) {
    fun startWindDown() = mInteractor.turnOn()

    fun endWindDown() = mInteractor.turnOff()
}