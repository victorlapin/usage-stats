package com.victorlapin.usagestats.view

import moxy.MvpView
import moxy.viewstate.strategy.AddToEndSingleStrategy
import moxy.viewstate.strategy.SkipStrategy
import moxy.viewstate.strategy.StateStrategyType

interface HomeFragmentView : MvpView {
    @StateStrategyType(SkipStrategy::class)
    fun showFirstRun()

    @StateStrategyType(SkipStrategy::class)
    fun showServiceToast()

    @StateStrategyType(SkipStrategy::class)
    fun showTapTarget()

    @StateStrategyType(AddToEndSingleStrategy::class)
    fun invalidatePager(daysToShow: Int)
}