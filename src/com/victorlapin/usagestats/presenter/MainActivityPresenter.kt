package com.victorlapin.usagestats.presenter

import com.victorlapin.usagestats.Const
import com.victorlapin.usagestats.HomeScreen
import com.victorlapin.usagestats.SettingsServicesScreen
import com.victorlapin.usagestats.SettingsWindDownScreen
import com.victorlapin.usagestats.manager.ServicesManager
import com.victorlapin.usagestats.model.interactor.WorkerInteractor
import com.victorlapin.usagestats.view.MainActivityView
import io.reactivex.Completable
import moxy.InjectViewState
import moxy.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class MainActivityPresenter constructor(
    private val mServices: ServicesManager,
    private val mRouter: Router,
    private val mWorkerInteractor: WorkerInteractor
) : MvpPresenter<MainActivityView>() {
    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        mWorkerInteractor.checkPeriodicWorkerScheduled()
            .flatMapCompletable {
                if (it) {
                    Completable.complete()
                } else {
                    mWorkerInteractor.schedulePeriodicWorker()
                }
            }
            .subscribe()
        mWorkerInteractor.checkDatabaseHygieneWorkerScheduled()
            .flatMapCompletable {
                if (it) {
                    Completable.complete()
                } else {
                    mWorkerInteractor.scheduleDatabaseHygieneWorker()
                }
            }
            .subscribe()
    }

    fun showFragment(screenKey: String?) {
        if (screenKey != null) {
            when (screenKey) {
                Const.FRAGMENT_SETTINGS_SERVICES -> mRouter.newRootScreen(SettingsServicesScreen())
                Const.FRAGMENT_SETTINGS_WIND_DOWN -> mRouter.newRootScreen(SettingsWindDownScreen())
            }
            mServices.reportShortcutUsed(screenKey)
        } else {
            mRouter.newRootScreen(HomeScreen())
        }
    }
}