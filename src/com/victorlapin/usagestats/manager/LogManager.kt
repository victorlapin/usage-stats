package com.victorlapin.usagestats.manager

import com.victorlapin.usagestats.BuildConfig
import com.victorlapin.usagestats.FileTree
import timber.log.Timber

class LogManager(
    private val mSettings: SettingsManager
) {
    private val mTree = FileTree()

    fun onStartup() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        if (mSettings.enableFileLog) {
            try {
                enableFileLogs()
            } catch (ex: Exception) {
                disableFileLogs()
                Timber.e(ex)
            }
        }
    }

    fun enableFileLogs() {
        Timber.plant(mTree)
    }

    fun disableFileLogs() {
        if (Timber.forest().contains(mTree)) {
            Timber.uproot(mTree)
        }
    }
}