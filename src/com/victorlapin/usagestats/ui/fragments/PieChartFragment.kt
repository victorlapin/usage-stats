package com.victorlapin.usagestats.ui.fragments

import android.graphics.Typeface
import android.os.Bundle
import android.view.animation.DecelerateInterpolator
import android.widget.Toast
import com.afollestad.assent.Permission
import com.afollestad.assent.runWithPermissions
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.amulyakhare.textdrawable.util.ColorGenerator
import com.getkeepsafe.taptargetview.TapTarget
import com.getkeepsafe.taptargetview.TapTargetSequence
import com.razerdp.widget.animatedpieview.AnimatedPieViewConfig
import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.di.PROP_STATS_MODE_CHANGED
import com.victorlapin.usagestats.manager.ResourcesManager
import com.victorlapin.usagestats.model.AppsUsage
import com.victorlapin.usagestats.model.SystemUsageStats
import com.victorlapin.usagestats.model.UsagePieInfo
import com.victorlapin.usagestats.presenter.PieChartFragmentPresenter
import com.victorlapin.usagestats.view.PieChartFragmentView
import com.victorlapin.usagestats.visible
import com.victorlapin.usagestats.visibleIn
import kotlinx.android.synthetic.main.fragment_pie.*
import kotlinx.android.synthetic.main.include_progress.*
import moxy.ktx.moxyPresenter
import org.koin.android.ext.android.getKoin
import org.koin.android.ext.android.inject
import org.koin.androidx.scope.currentScope
import org.koin.core.parameter.parametersOf
import java.util.concurrent.TimeUnit

class PieChartFragment : BaseFragment(), PieChartFragmentView {
    override val layoutRes = R.layout.fragment_pie

    private val mDateOffset: Int by lazy {
        arguments!!.getInt(ARG_DATE_OFFSET)
    }

    private val presenter by moxyPresenter {
        currentScope.get<PieChartFragmentPresenter>(parameters = { parametersOf(mDateOffset) })
    }

    private val mResources by inject<ResourcesManager>()
    private val mTotalHoursString = mResources.getString(R.string.total_hours)
    private val mTotalMinutesString = mResources.getString(R.string.total_minutes)
    private val mTodayString = mResources.getString(R.string.today)
    private val mYesterdayString = mResources.getString(R.string.yesterday)

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        btn_manual_save.setOnClickListener {
            MaterialDialog(context!!).show {
                lifecycleOwner(this@PieChartFragment)
                title(R.string.action_manual_save)
                message(R.string.action_manual_save_summary)
                cancelable(true)
                negativeButton(R.string.later)
                positiveButton(R.string.action_save) { presenter.manualSaveStats() }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (getKoin().getProperty(PROP_STATS_MODE_CHANGED)!!) {
            getKoin().setProperty(PROP_STATS_MODE_CHANGED, false)
            presenter.getData()
        }
    }

    override fun setData(data: SystemUsageStats) {
        chart.post {
            try {
                lbl_date.text = when (data.dateBuilder.offset) {
                    0 -> mTodayString
                    -1 -> mYesterdayString
                    else -> data.dateBuilder.formattedDate
                }

                lbl_unlocks_count.text = data.unlocksCount.toString()
                lbl_notifications_count.text = data.notificationsCount.toString()
                if (data.notificationsCount > 0) {
                    notifications_container.setOnClickListener { presenter.openNotifications() }
                } else {
                    notifications_container.setOnClickListener(null)
                }
                if (data.unlocksCount > 0) {
                    unlocks_container.setOnClickListener { presenter.openUnlocks() }
                } else {
                    unlocks_container.setOnClickListener(null)
                }

                var totalDuration = 0L
                val config = getPieDefaultConfig()

                if (data.appsUsage.data.isEmpty()) {
                    config.animatePie(false)
                        .drawText(false)
                        .canTouch(false)
                        .addData(
                            UsagePieInfo(
                                10F.toDouble(),
                                mResources.getColor(android.R.color.darker_gray),
                                false,
                                mResources.getString(R.string.no_data)
                            )
                        )
                } else {
                    config.animatePie(data.appsUsage.source == AppsUsage.SOURCE_SYSTEM)
                        .drawText(true)
                        .canTouch(true)
                    data.appsUsage.data.forEach {
                        val info = UsagePieInfo(
                            it.usage.toDouble(),
                            ColorGenerator.MATERIAL.randomColor,
                            it.isDeleted,
                            it.appName,
                            it.image
                        )
                        config.addData(info)
                        totalDuration += it.usage
                    }
                }
                chart.start(config)

                // calculate total
                if (totalDuration > 0) {
                    val sb = StringBuilder()
                    val hours = totalDuration / TimeUnit.HOURS.toMillis(1)
                    if (hours > 0) {
                        sb.append(mTotalHoursString.format(hours))
                        sb.append(" ")
                        totalDuration -= hours * TimeUnit.HOURS.toMillis(1)
                    }
                    val minutes = totalDuration / TimeUnit.MINUTES.toMillis(1)
                    sb.append(mTotalMinutesString.format(minutes))
                    lbl_total.text = sb.toString()
                    total_container.setOnClickListener { presenter.openApps() }
                } else {
                    lbl_total.text = mTotalMinutesString.format(0)
                    total_container.setOnClickListener(null)
                }
            } catch (ignore: Exception) {
            }
        }
    }

    override fun showManualSaveButton(isVisible: Boolean) {
        btn_manual_save.visible(isVisible)
    }

    override fun enableManualSaveButton(isEnabled: Boolean) {
        btn_manual_save.isEnabled = isEnabled
    }

    override fun toggleProgress(isVisible: Boolean) {
        btn_manual_save.isEnabled = !isVisible
        progress_bar_layout.visible(isVisible)
    }

    override fun toggleNetworkProgress(isVisible: Boolean) {
        progress_traffic.visible(isVisible)
        lbl_traffic_count.visibleIn(!isVisible)
    }

    override fun setNetworkData(bytes: Long) {
        lbl_traffic_count.text = mResources.formatTrafficSpannable(bytes)
        if (bytes > 0) {
            traffic_container.setOnClickListener {
                runWithPermissions(Permission.READ_PHONE_STATE) {
                    presenter.openTraffic()
                }
            }
        } else {
            traffic_container.setOnClickListener(null)
        }
    }

    fun showTapTarget() {
        TapTargetSequence(activity!!).targets(
            TapTarget.forView(
                total_container,
                mResources.getString(R.string.target_total),
                mResources.getString(R.string.target_total_desc)
            ).transparentTarget(false)
                .cancelable(false),
            TapTarget.forView(
                unlocks_container,
                mResources.getString(R.string.target_unlocks),
                mResources.getString(R.string.target_unlocks_desc)
            ).transparentTarget(false)
                .cancelable(false),
            TapTarget.forView(
                traffic_container,
                mResources.getString(R.string.target_network),
                mResources.getString(R.string.target_network_desc)
            ).transparentTarget(false)
                .cancelable(false),
            TapTarget.forView(
                notifications_container,
                mResources.getString(R.string.target_notifications),
                mResources.getString(R.string.target_notifications_desc)
            ).transparentTarget(false)
                .cancelable(false),
            TapTarget.forView(
                tutorial_swipe_target,
                mResources.getString(R.string.target_prev),
                mResources.getString(R.string.target_prev_desc)
            ).transparentTarget(false)
                .icon(mResources.getDrawable(R.drawable.gesture_swipe_right))
                .tintTarget(true)
                .cancelable(false)
        ).start()
    }

    private fun getPieDefaultConfig(): AnimatedPieViewConfig {
        return AnimatedPieViewConfig()
            .strokeMode(true)
            .strokeWidth(8)
            .duration(1000)
            .startAngle(0F)
            .textSize(32F)
            .textMargin(8)
            .splitAngle(0F)
            .interpolator(DecelerateInterpolator())
            .autoSize(true)
            .pieRadiusRatio(1F)
            .animOnTouch(true)
            .focusAlphaType(AnimatedPieViewConfig.FOCUS_WITH_ALPHA_REV)
            .focusAlpha(100)
            .floatShadowRadius(4F)
            .floatExpandSize(4F)
            .typeFae(Typeface.create("sans-serif-condensed", Typeface.NORMAL))
            .guidePointRadius(0)
            .cubicGuide(true)
            .selectListener { pieInfo: UsagePieInfo, isFloatUp ->
                if (!pieInfo.appName.isNullOrEmpty() && isFloatUp) {
                    Toast.makeText(context, pieInfo.appName, Toast.LENGTH_SHORT).show()
                }
            }
    }

    companion object {
        private const val ARG_DATE_OFFSET = "arg_date_offset"

        fun newInstance(dateOffset: Int): PieChartFragment {
            val args = Bundle().apply {
                putInt(ARG_DATE_OFFSET, dateOffset)
            }

            return PieChartFragment().apply {
                arguments = args
            }
        }
    }
}