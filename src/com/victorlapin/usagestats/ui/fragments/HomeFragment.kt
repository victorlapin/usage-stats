package com.victorlapin.usagestats.ui.fragments

import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.OnLifecycleEvent
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.presenter.HomeFragmentPresenter
import com.victorlapin.usagestats.ui.TodayEventBridge
import com.victorlapin.usagestats.ui.adapters.PieChartAdapter
import com.victorlapin.usagestats.view.HomeFragmentView
import io.reactivex.disposables.Disposable
import kotlinx.android.synthetic.main.fragment_home.*
import moxy.ktx.moxyPresenter
import org.koin.androidx.scope.currentScope

class HomeFragment : BaseFragment(), HomeFragmentView {
    override val layoutRes = R.layout.fragment_home

    private val presenter by moxyPresenter { currentScope.get<HomeFragmentPresenter>() }

    private val mTodayBridge by currentScope.inject<TodayEventBridge>()
    private var mDisposable: Disposable? = null

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        pager.offscreenPageLimit = 1
        coordinator.postDelayed({ list.scrollTo(0, 0) }, 100)
    }

    override fun invalidatePager(daysToShow: Int) {
        if (pager.adapter == null ||
            (pager.adapter as PieChartAdapter).itemCount != daysToShow + 1) {
            val pieAdapter = PieChartAdapter(this, daysToShow)
            pager.apply {
                adapter = pieAdapter
            }
        }
    }

    override fun showFirstRun() {
        MaterialDialog(context!!).show {
            lifecycleOwner(this@HomeFragment)
            title(R.string.first_run_title)
            message(R.string.first_run_text)
            cancelable(false)
            setOnDismissListener { presenter.disableFirstRun() }
            negativeButton(R.string.later) { presenter.checkTapTarget() }
            positiveButton(R.string.action_settings) { presenter.openServices() }
        }
    }

    override fun showServiceToast() {
        Toast.makeText(
            context, R.string.service_screen_unlock_warning,
            Toast.LENGTH_SHORT
        ).show()
    }

    override fun showTapTarget() {
        val tag = "f${pager.currentItem}"
        val fragment = childFragmentManager.findFragmentByTag(tag)
        fragment?.let {
            (it as PieChartFragment).showTapTarget()
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_RESUME)
    private fun subscribeToBridge() {
        mDisposable = mTodayBridge.todayClickedEvent.subscribe {
            pager.setCurrentItem(0, false)
        }
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_PAUSE)
    private fun unsubscribeFromBridge() {
        mDisposable?.dispose()
        mDisposable = null
    }

    companion object {
        fun newInstance() = HomeFragment()
    }
}