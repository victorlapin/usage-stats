package com.victorlapin.usagestats.manager

import android.Manifest
import android.Manifest.permission.PACKAGE_USAGE_STATS
import android.annotation.SuppressLint
import android.app.ActivityManager
import android.app.AlarmManager
import android.app.AppOpsManager
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.usage.NetworkStatsManager
import android.app.usage.UsageStatsManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.ShortcutManager
import android.os.Build
import android.provider.Settings
import android.telephony.TelephonyManager
import androidx.core.app.NotificationManagerCompat
import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.ui.receivers.BootReceiver
import com.victorlapin.usagestats.ui.receivers.WindDownBootReceiver
import com.victorlapin.usagestats.ui.receivers.WindDownEndReceiver
import com.victorlapin.usagestats.ui.receivers.WindDownStartReceiver
import com.victorlapin.usagestats.ui.services.ScreenUnlockService

class ServicesManager(
    private val mContext: Context
) {
    private val notificationManager by lazy {
        mContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
    }

    val usageStatsManager by lazy {
        mContext.getSystemService(Context.USAGE_STATS_SERVICE) as UsageStatsManager
    }

    val networkStatsManager by lazy {
        mContext.getSystemService(Context.NETWORK_STATS_SERVICE) as NetworkStatsManager
    }

    val packageManager: PackageManager by lazy {
        mContext.packageManager
    }

    fun startUnlocksService() {
        val intent = Intent(mContext, ScreenUnlockService::class.java)
        intent.action = ScreenUnlockService.ACTION_START
        mContext.startService(intent)
    }

    fun stopUnlocksService() {
        val intent = Intent(mContext, ScreenUnlockService::class.java)
        intent.action = ScreenUnlockService.ACTION_STOP
        mContext.startService(intent)
    }

    private val appOpsManager by lazy {
        mContext.getSystemService(Context.APP_OPS_SERVICE) as AppOpsManager
    }

    fun isPackageStatsGranted(): Boolean {
        val granted: Boolean
        val mode = appOpsManager.checkOpNoThrow(
            AppOpsManager.OPSTR_GET_USAGE_STATS,
            android.os.Process.myUid(), selfPackageName
        )

        granted = if (mode == AppOpsManager.MODE_DEFAULT) {
            mContext.checkCallingOrSelfPermission(PACKAGE_USAGE_STATS) ==
                    PackageManager.PERMISSION_GRANTED
        } else {
            mode == AppOpsManager.MODE_ALLOWED
        }

        return granted
    }

    fun isNotificationsAccessGranted(): Boolean =
        NotificationManagerCompat.getEnabledListenerPackages(mContext)
            .any { it == selfPackageName }

    val selfPackageName: String = mContext.packageName

    fun enableBootReceiver() {
        val receiver = ComponentName(mContext, BootReceiver::class.java)
        packageManager.setComponentEnabledSetting(
            receiver,
            PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
            PackageManager.DONT_KILL_APP
        )
    }

    fun disableBootReceiver() {
        val receiver = ComponentName(mContext, BootReceiver::class.java)
        packageManager.setComponentEnabledSetting(
            receiver,
            PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
            PackageManager.DONT_KILL_APP
        )
    }

    fun enableWindDownBootReceiver() {
        val receiver = ComponentName(mContext, WindDownBootReceiver::class.java)
        packageManager.setComponentEnabledSetting(
            receiver,
            PackageManager.COMPONENT_ENABLED_STATE_ENABLED,
            PackageManager.DONT_KILL_APP
        )
    }

    fun disableWindDownBootReceiver() {
        val receiver = ComponentName(mContext, WindDownBootReceiver::class.java)
        packageManager.setComponentEnabledSetting(
            receiver,
            PackageManager.COMPONENT_ENABLED_STATE_DISABLED,
            PackageManager.DONT_KILL_APP
        )
    }

    private val activityManager by lazy {
        mContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
    }

    fun isUnlocksServiceRunning(): Boolean = activityManager.runningAppProcesses
        .filter { it.processName.startsWith(selfPackageName) }
        .any { it.processName.endsWith(":unlocks") }

    private val shortcutManager by lazy {
        if (Build.VERSION.SDK_INT >= 25) {
            mContext.getSystemService(Context.SHORTCUT_SERVICE) as ShortcutManager
        } else {
            null
        }
    }

    fun reportShortcutUsed(shortcutId: String) {
        if (Build.VERSION.SDK_INT >= 25) {
            shortcutManager?.reportShortcutUsed(shortcutId)
        }
    }

    fun createNotificationChannels() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channelId = mContext.getString(R.string.channel_default_id)
            if (notificationManager.getNotificationChannel(channelId) == null) {
                val channel = NotificationChannel(
                    channelId,
                    mContext.getString(R.string.channel_default_title),
                    NotificationManager.IMPORTANCE_LOW
                )
                channel.setShowBadge(false)
                notificationManager.createNotificationChannel(channel)
            }
        }
    }

    val alarmManager by lazy {
        mContext.getSystemService(Context.ALARM_SERVICE) as AlarmManager
    }

    val windDownStartIntent: PendingIntent by lazy {
        val intent = Intent(mContext, WindDownStartReceiver::class.java)
        PendingIntent.getBroadcast(
            mContext, WIND_DOWN_START_REQUEST_CODE, intent,
            PendingIntent.FLAG_CANCEL_CURRENT
        )
    }

    val windDownEndIntent: PendingIntent by lazy {
        val intent = Intent(mContext, WindDownEndReceiver::class.java)
        PendingIntent.getBroadcast(
            mContext, WIND_DOWN_END_REQUEST_CODE, intent,
            PendingIntent.FLAG_CANCEL_CURRENT
        )
    }

    val windDownPendingEndIntent: PendingIntent by lazy {
        val intent = Intent(mContext, WindDownEndReceiver::class.java)
        PendingIntent.getBroadcast(
            mContext, WIND_DOWN_PENDING_END_REQUEST_CODE, intent,
            PendingIntent.FLAG_CANCEL_CURRENT
        )
    }

    fun isSecureSettingsGranted(): Boolean =
        mContext.checkCallingOrSelfPermission(SECURE_SETTINGS_PERMISSION) ==
                PackageManager.PERMISSION_GRANTED

    fun isGrayscaled(): Boolean =
        Settings.Secure.getInt(
            mContext.contentResolver,
            DISPLAY_DALTONIZER_ENABLED,
            0
        ) == 1 &&
                Settings.Secure.getInt(
                    mContext.contentResolver,
                    DISPLAY_DALTONIZER,
                    -1
                ) == 0

    fun setGrayscale(isEnabled: Boolean) {
        Settings.Secure.putInt(
            mContext.contentResolver,
            DISPLAY_DALTONIZER_ENABLED,
            if (isEnabled) 1 else 0
        )
        Settings.Secure.putInt(
            mContext.contentResolver,
            DISPLAY_DALTONIZER,
            if (isEnabled) 0 else -1
        )
    }

    fun setDnd(mode: Int) = notificationManager.setInterruptionFilter(mode)

    fun isNotificationPolicyAccessGranted(): Boolean =
        notificationManager.isNotificationPolicyAccessGranted

    val adbCommand = "adb shell pm grant ${mContext.packageName} $SECURE_SETTINGS_PERMISSION"

    val suCommand = "su -c pm grant ${mContext.packageName} $SECURE_SETTINGS_PERMISSION"

    private val telephonyManager by lazy {
        mContext.getSystemService(Context.TELEPHONY_SERVICE) as TelephonyManager
    }

    @SuppressLint("MissingPermission", "HardwareIds")
    fun getSubscriberId(): String =
        if (isPhoneStateGranted()) telephonyManager.subscriberId ?: "" else ""

    fun isPhoneStateGranted(): Boolean =
        mContext.checkCallingOrSelfPermission(Manifest.permission.READ_PHONE_STATE) ==
                PackageManager.PERMISSION_GRANTED

    companion object {
        private const val SECURE_SETTINGS_PERMISSION = "android.permission.WRITE_SECURE_SETTINGS"
        private const val WIND_DOWN_START_REQUEST_CODE = 300
        private const val WIND_DOWN_END_REQUEST_CODE = 301
        private const val WIND_DOWN_PENDING_END_REQUEST_CODE = 302
        private const val DISPLAY_DALTONIZER_ENABLED = "accessibility_display_daltonizer_enabled"
        private const val DISPLAY_DALTONIZER = "accessibility_display_daltonizer"
    }
}