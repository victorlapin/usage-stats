package com.victorlapin.usagestats.model.interactor

import com.victorlapin.usagestats.model.repository.AboutRepository

class AboutInteractor(private val mRepo: AboutRepository) {
    fun getData() = mRepo.getData()
}