package com.victorlapin.usagestats.ui.fragments

import android.app.NotificationManager
import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.datetime.timePicker
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.manager.ServicesManager
import com.victorlapin.usagestats.manager.SettingsManager
import com.victorlapin.usagestats.model.WindDownDateBuilder
import com.victorlapin.usagestats.model.interactor.WindDownInteractor
import org.koin.android.ext.android.inject
import java.util.*

class SettingsWindDownFragment : PreferenceFragmentCompat() {
    private val mSettings by inject<SettingsManager>()
    private val mServices by inject<ServicesManager>()
    private val mWindDownInteractor by inject<WindDownInteractor>()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences_wind_down)

        val startTimePref = findPreference<Preference>(SettingsManager.KEY_WIND_DOWN_START)!!
        startTimePref.summary =
            WindDownDateBuilder.timeFormatter.format(mSettings.windDownStartTime)
        startTimePref.setOnPreferenceClickListener {
            val defCal = Calendar.getInstance(TimeZone.getDefault()).apply {
                timeInMillis = mSettings.windDownStartTime
            }
            MaterialDialog(context!!).show {
                lifecycleOwner(this@SettingsWindDownFragment)
                timePicker(
                    currentTime = defCal,
                    show24HoursView = android.text.format.DateFormat.is24HourFormat(context)
                ) { _, datetime ->
                    mSettings.windDownStartTime = datetime.apply {
                        set(Calendar.SECOND, 0)
                        set(Calendar.MILLISECOND, 0)
                    }.timeInMillis
                    it.summary =
                        WindDownDateBuilder.timeFormatter.format(mSettings.windDownStartTime)
                    if (mSettings.isWindDownEnabled) {
                        reschedule()
                    }
                }
                positiveButton(res = android.R.string.ok)
                negativeButton(res = android.R.string.cancel)
            }
            return@setOnPreferenceClickListener true
        }

        val endTimePref = findPreference<Preference>(SettingsManager.KEY_WIND_DOWN_END)!!
        endTimePref.summary =
            WindDownDateBuilder.timeFormatter.format(mSettings.windDownEndTime)
        endTimePref.setOnPreferenceClickListener {
            val defCal = Calendar.getInstance(TimeZone.getDefault()).apply {
                timeInMillis = mSettings.windDownEndTime
            }
            MaterialDialog(context!!).show {
                lifecycleOwner(this@SettingsWindDownFragment)
                timePicker(
                    currentTime = defCal,
                    show24HoursView = android.text.format.DateFormat.is24HourFormat(context)
                ) { _, datetime ->
                    mSettings.windDownEndTime = datetime.apply {
                        set(Calendar.SECOND, 0)
                        set(Calendar.MILLISECOND, 0)
                    }.timeInMillis
                    it.summary =
                        WindDownDateBuilder.timeFormatter.format(mSettings.windDownEndTime)
                    if (mSettings.isWindDownEnabled) {
                        reschedule()
                    }
                }
                positiveButton(res = android.R.string.ok)
                negativeButton(res = android.R.string.cancel)
            }
            return@setOnPreferenceClickListener true
        }

        findPreference<SwitchPreference>(SettingsManager.KEY_WIND_DOWN_ENABLED)!!.setOnPreferenceChangeListener { _, newValue ->
            if (newValue as Boolean) {
                reschedule()
            } else {
                if (mSettings.isWindDownNow) {
                    mWindDownInteractor.turnOff()
                }
                mWindDownInteractor.cancelWindDown().subscribe()
            }
            return@setOnPreferenceChangeListener true
        }

        val grayscalePref =
            findPreference<SwitchPreference>(SettingsManager.KEY_WIND_DOWN_GRAYSCALE)!!
        grayscalePref.isEnabled = mServices.isSecureSettingsGranted()
        if (!grayscalePref.isEnabled) {
            grayscalePref.summary = context!!.getString(R.string.needs_secure_settings)
        }

        val dndPref = findPreference<SwitchPreference>(SettingsManager.KEY_WIND_DOWN_DND)!!
        dndPref.isEnabled = mServices.isNotificationPolicyAccessGranted()
        if (!dndPref.isEnabled) {
            dndPref.summary = context!!.getString(R.string.needs_notification_access)
        }

        val dndModePref = findPreference<Preference>(SettingsManager.KEY_WIND_DOWN_DND_MODE)!!
        val dndModeValues = arrayOf(
            NotificationManager.INTERRUPTION_FILTER_ALARMS,
            NotificationManager.INTERRUPTION_FILTER_PRIORITY
        )
        val dndModeStrings = listOf(
            context!!.getString(R.string.dnd_alarms),
            context!!.getString(R.string.dnd_priority)
        )
        dndModePref.summary = dndModeStrings[dndModeValues.indexOf(mSettings.windDownDndMode)]
        dndModePref.setOnPreferenceClickListener {
            MaterialDialog(context!!).show {
                lifecycleOwner(this@SettingsWindDownFragment)
                title(res = R.string.pref_wind_down_mode_title)
                listItemsSingleChoice(
                    items = dndModeStrings,
                    initialSelection = dndModeValues.indexOf(mSettings.windDownDndMode)
                ) { _, index, _ ->
                    mSettings.windDownDndMode = dndModeValues[index]
                    it.summary = dndModeStrings[dndModeValues.indexOf(mSettings.windDownDndMode)]
                }
                negativeButton(res = android.R.string.cancel)
                positiveButton(res = android.R.string.ok)
            }
            return@setOnPreferenceClickListener true
        }
    }

    private fun reschedule() {
        val dateBuilder =
            WindDownDateBuilder(mSettings.windDownStartTime, mSettings.windDownEndTime)
        if (dateBuilder.isInRangeNow()) {
            mWindDownInteractor.turnOn()
            mWindDownInteractor.scheduleExitFromPendingWindDown(dateBuilder.previousEndAlarmTime)
        } else if (mSettings.isWindDownNow) {
            mWindDownInteractor.turnOff()
        }
        mWindDownInteractor.scheduleWindDown(dateBuilder.nextStartAlarmTime,
            dateBuilder.nextEndAlarmTime).subscribe()
    }

    companion object {
        fun newInstance() = SettingsWindDownFragment()
    }
}