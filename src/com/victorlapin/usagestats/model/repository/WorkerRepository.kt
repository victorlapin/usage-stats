package com.victorlapin.usagestats.model.repository

import android.content.Context
import androidx.work.ExistingPeriodicWorkPolicy
import androidx.work.ExistingWorkPolicy
import androidx.work.WorkManager
import com.victorlapin.usagestats.work.AppUsageWorker
import com.victorlapin.usagestats.work.DatabaseHygieneWorker
import com.victorlapin.usagestats.work.PeriodicWorker
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.text.DateFormat
import java.text.SimpleDateFormat

class WorkerRepository(
    private val mContext: Context
) {
    private val mTag = "WorkerRepository"
    private val mFormatter = SimpleDateFormat
        .getDateTimeInstance(DateFormat.MEDIUM, DateFormat.MEDIUM)

    private fun checkWorker(name: String): Single<Boolean> = Single.create<Boolean> { emitter ->
        val statusList = WorkManager.getInstance(mContext)
            .getWorkInfosForUniqueWork(name)
            .get()
        if (statusList.isNotEmpty()) {
            val state = statusList.last().state
            Timber.tag(mTag).i("$name check, current state: $state")
            emitter.onSuccess(!state.isFinished)
        } else {
            Timber.tag(mTag).i("$name check, not found")
            emitter.onSuccess(false)
        }
    }

    fun scheduleAppUsageWorker(runTime: Long): Completable =
        Completable.create { emitter ->
            WorkManager.getInstance(mContext)
                .beginUniqueWork(
                    AppUsageWorker.WORK_TAG,
                    ExistingWorkPolicy.REPLACE,
                    AppUsageWorker.buildRequest(runTime)
                )
                .enqueue()
            Timber.tag(mTag).i("App usage worker scheduled at ${mFormatter.format(runTime)}")
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())

    fun checkAppUsageWorkerScheduled(): Single<Boolean> = checkWorker(AppUsageWorker.WORK_TAG)

    fun schedulePeriodicWorker(): Completable =
        Completable.create { emitter ->
            WorkManager.getInstance(mContext)
                .enqueueUniquePeriodicWork(
                    PeriodicWorker.WORK_TAG,
                    ExistingPeriodicWorkPolicy.REPLACE,
                    PeriodicWorker.buildRequest()
                )
            Timber.tag(mTag).i("Periodic worker scheduled")
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())

    fun checkPeriodicWorkerScheduled(): Single<Boolean> = checkWorker(PeriodicWorker.WORK_TAG)

    fun cancelAppUsageWorker(): Completable = Completable.create { emitter ->
        WorkManager.getInstance(mContext)
            .cancelUniqueWork(AppUsageWorker.WORK_TAG)
        Timber.tag(mTag).i("App usage worker canceled")
        emitter.onComplete()
    }

    fun cancelPeriodicWorker(): Completable = Completable.create { emitter ->
        WorkManager.getInstance(mContext)
            .cancelUniqueWork(PeriodicWorker.WORK_TAG)
        Timber.tag(mTag).i("Periodic worker canceled")
        emitter.onComplete()
    }

    fun scheduleDatabaseHygieneWorker(): Completable =
        Completable.create { emitter ->
            WorkManager.getInstance(mContext)
                .enqueueUniquePeriodicWork(
                    DatabaseHygieneWorker.WORK_TAG,
                    ExistingPeriodicWorkPolicy.REPLACE,
                    DatabaseHygieneWorker.buildRequest()
                )
            Timber.tag(mTag).i("Database hygiene worker scheduled")
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())

    fun checkDatabaseHygieneWorkerScheduled(): Single<Boolean> =
        checkWorker(DatabaseHygieneWorker.WORK_TAG)

    fun cancelDatabaseHygieneWorker(): Completable = Completable.create { emitter ->
        WorkManager.getInstance(mContext)
            .cancelUniqueWork(DatabaseHygieneWorker.WORK_TAG)
        Timber.tag(mTag).i("Database hygiene worker canceled")
        emitter.onComplete()
    }
}