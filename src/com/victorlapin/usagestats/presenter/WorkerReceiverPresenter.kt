package com.victorlapin.usagestats.presenter

import com.victorlapin.usagestats.model.interactor.WorkerInteractor

class WorkerReceiverPresenter(
    private val mWorkerInteractor: WorkerInteractor
) {
    fun scheduleAppUsageWorker() {
        mWorkerInteractor.scheduleAppUsageWorker().subscribe()
    }
}