package com.victorlapin.usagestats.work

import android.content.Context
import androidx.work.Constraints
import androidx.work.PeriodicWorkRequest
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import com.victorlapin.usagestats.manager.SettingsManager
import com.victorlapin.usagestats.model.DateBuilder
import com.victorlapin.usagestats.model.interactor.AppUsageInteractor
import com.victorlapin.usagestats.model.interactor.CollectedEventsInteractor
import com.victorlapin.usagestats.model.interactor.NetworkUsageInteractor
import io.reactivex.Single
import io.reactivex.functions.Function3
import org.koin.core.KoinComponent
import org.koin.core.inject
import timber.log.Timber
import java.util.concurrent.TimeUnit

class DatabaseHygieneWorker(context: Context, params: WorkerParameters) :
    RxWorker(context, params), KoinComponent {
    private val mAppsInteractor by inject<AppUsageInteractor>()
    private val mEventsInteractor by inject<CollectedEventsInteractor>()
    private val mNetworkInteractor by inject<NetworkUsageInteractor>()
    private val mSettings by inject<SettingsManager>()

    override fun createWork(): Single<Result> = Single
        .just(DateBuilder(mSettings.daysToKeep * -1))
        .flatMap { dateBuilder ->
            Single.zip(mAppsInteractor.deleteOldStats(dateBuilder.dateFrom),
                mEventsInteractor.deleteOldEvents(dateBuilder.dateFrom),
                mNetworkInteractor.deleteOldStats(dateBuilder.dateFrom),
                Function3<Int, Int, Int, Any> { rows1, rows2, rows3 ->
                    Timber.tag(WORK_TAG).i("Rows deleted: ${rows1 + rows2 + rows3}")
                    Any()
                })
        }
        .flatMapCompletable { mEventsInteractor.vacuum() }
        .toSingleDefault(Result.success())
        .onErrorReturnItem(Result.retry())
        .doOnSubscribe {
            Timber.tag(WORK_TAG).i("Worker started")
        }
        .doOnError { Timber.tag(WORK_TAG).e(it) }
        .doFinally {
            Timber.tag(WORK_TAG).i("Worker finished")
        }

    companion object {
        const val WORK_TAG = "DatabaseHygieneWorker"

        fun buildRequest(): PeriodicWorkRequest {
            val constraints = Constraints.Builder()
                .setRequiresBatteryNotLow(true)
                .setRequiresDeviceIdle(true)
                .build()

            return PeriodicWorkRequest.Builder(
                DatabaseHygieneWorker::class.java,
                1L, TimeUnit.DAYS
            )
                .setConstraints(constraints)
                .build()
        }
    }
}