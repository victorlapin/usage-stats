package com.victorlapin.usagestats.view

import com.victorlapin.usagestats.model.SystemUsageStats
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.StateStrategyType

interface PieChartFragmentView : MvpView {
    @StateStrategyType(AddToEndStrategy::class)
    fun setData(data: SystemUsageStats)

    @StateStrategyType(AddToEndStrategy::class)
    fun showManualSaveButton(isVisible: Boolean)

    @StateStrategyType(AddToEndStrategy::class)
    fun enableManualSaveButton(isEnabled: Boolean)

    @StateStrategyType(AddToEndStrategy::class)
    fun toggleProgress(isVisible: Boolean)

    @StateStrategyType(AddToEndStrategy::class)
    fun toggleNetworkProgress(isVisible: Boolean)

    @StateStrategyType(AddToEndStrategy::class)
    fun setNetworkData(bytes: Long)
}