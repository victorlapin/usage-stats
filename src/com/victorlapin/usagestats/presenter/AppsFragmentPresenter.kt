package com.victorlapin.usagestats.presenter

import com.victorlapin.usagestats.AppDetailsScreen
import com.victorlapin.usagestats.model.DateBuilder
import com.victorlapin.usagestats.model.interactor.AppUsageInteractor
import com.victorlapin.usagestats.view.AppsFragmentView
import io.reactivex.disposables.Disposable
import moxy.InjectViewState
import moxy.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class AppsFragmentPresenter(
    private val mInteractor: AppUsageInteractor,
    private val mRouter: Router,
    private val mDateOffset: Int
) : MvpPresenter<AppsFragmentView>() {
    override fun attachView(view: AppsFragmentView?) {
        super.attachView(view)
        getData()
    }

    private fun getData() {
        val dateBuilder = DateBuilder(mDateOffset)
        var disposable: Disposable? = null
        disposable = mInteractor.getDetailedStats(dateBuilder)
            .doOnSubscribe { viewState.toggleProgress(true) }
            .doFinally {
                disposable?.dispose()
                viewState.toggleProgress(false)
            }
            .subscribe({
                viewState.setData(it.data)
            }, {
                it.printStackTrace()
            })
    }

    fun onBackPressed() = mRouter.exit()

    fun openAppDetails(packageName: String) =
        mRouter.navigateTo(AppDetailsScreen(packageName))
}