package com.victorlapin.usagestats.model

import android.graphics.drawable.Drawable
import androidx.room.Ignore
import com.victorlapin.usagestats.model.database.entity.AppUsageStats
import com.victorlapin.usagestats.model.database.entity.NetworkUsageStats
import ru.terrakok.cicerone.Screen

data class SystemUsageStats(
    val dateBuilder: DateBuilder,
    val unlocksCount: Int,
    val notificationsCount: Int,
    val appsUsage: AppsUsage
)

data class AppsUsage(val source: Int, val data: List<AppUsageStats>) {
    companion object {
        const val SOURCE_DB = 0
        const val SOURCE_SYSTEM = 1
    }
}

data class AboutClickEventArgs(val screen: Screen)

data class AggregatedEvent(var appName: String, val quantity: Int) {
    @Ignore
    var image: Drawable? = null
}

data class NetworkUsage(val source: Int, val data: List<NetworkUsageStats>) {
    companion object {
        const val SOURCE_DB = 0
        const val SOURCE_SYSTEM = 1
    }
}