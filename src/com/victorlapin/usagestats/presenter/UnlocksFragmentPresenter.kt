package com.victorlapin.usagestats.presenter

import com.victorlapin.usagestats.model.DateBuilder
import com.victorlapin.usagestats.model.interactor.CollectedEventsInteractor
import com.victorlapin.usagestats.view.UnlocksFragmentView
import io.reactivex.disposables.Disposable
import moxy.InjectViewState
import moxy.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class UnlocksFragmentPresenter(
    private val mInteractor: CollectedEventsInteractor,
    private val mRouter: Router,
    private val mDateOffset: Int
) : MvpPresenter<UnlocksFragmentView>() {
    override fun attachView(view: UnlocksFragmentView?) {
        super.attachView(view)
        getData()
    }

    private fun getData() {
        val dateBuilder = DateBuilder(mDateOffset)
        var disposable: Disposable? = null
        disposable = mInteractor.getUnlocks(dateBuilder.dateFrom, dateBuilder.dateTo)
            .doFinally { disposable?.dispose() }
            .subscribe({
                viewState.setData(it, dateBuilder.dateFrom, dateBuilder.dateTo)
            }, {
                it.printStackTrace()
            })
    }

    fun onBackPressed() = mRouter.exit()
}