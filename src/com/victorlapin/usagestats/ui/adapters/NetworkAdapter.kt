package com.victorlapin.usagestats.ui.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.inflate
import com.victorlapin.usagestats.manager.ResourcesManager
import com.victorlapin.usagestats.model.database.entity.NetworkUsageStats
import kotlinx.android.synthetic.main.item_network.view.*

class NetworkAdapter(
    private val mResources: ResourcesManager
) : RecyclerView.Adapter<NetworkAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val mItems = ArrayList<NetworkUsageStats>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(parent.inflate(R.layout.item_network))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mItems[position]
        holder.itemView.apply {
            container.alpha = if (item.isDeleted) 0.5f else 1f
            lbl_name.text = item.appName
            lbl_package.text = item.packageName
            image.setImageDrawable(
                item.image
                    ?: holder.itemView.context.getDrawable(R.drawable.android_head)
            )
            lbl_bytes_wifi.text = if (item.bytesWifiTx == item.bytesWifiRx) {
                context.getString(
                    R.string.traffic_wifi_zero,
                    mResources.formatTraffic(item.bytesWifiTx)
                )
            } else {
                context.getString(
                    R.string.traffic_wifi,
                    mResources.formatTraffic(item.bytesWifiTx),
                    mResources.formatTraffic(item.bytesWifiRx)
                )
            }
            lbl_bytes_mobile.text = if (item.bytesMobileTx == item.bytesMobileRx) {
                context.getString(
                    R.string.traffic_mobile_zero,
                    mResources.formatTraffic(item.bytesMobileTx)
                )
            } else {
                context.getString(
                    R.string.traffic_mobile,
                    mResources.formatTraffic(item.bytesMobileTx),
                    mResources.formatTraffic(item.bytesMobileRx)
                )
            }
        }
    }

    override fun getItemCount() = mItems.size

    fun setData(data: List<NetworkUsageStats>) {
        mItems.clear()
        mItems.addAll(data)
        notifyDataSetChanged()
    }
}