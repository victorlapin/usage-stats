package com.victorlapin.usagestats.model.database.dao

import androidx.room.*
import com.victorlapin.usagestats.model.database.entity.AppUsageStats
import io.reactivex.Single

@Dao
interface AppUsageStatsDao {
    @Query("select * from app_stats where stats_date = :dateFrom")
    fun getStats(dateFrom: Long): Single<List<AppUsageStats>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(stats: List<AppUsageStats>)

    @Transaction
    @Query("delete from app_stats")
    fun clear()

    @Transaction
    @Query("delete from app_stats where stats_date < :date")
    fun deleteOldStats(date: Long): Int
}