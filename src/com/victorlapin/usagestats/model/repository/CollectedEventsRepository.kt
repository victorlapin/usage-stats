package com.victorlapin.usagestats.model.repository

import com.victorlapin.usagestats.model.AggregatedEvent
import com.victorlapin.usagestats.model.database.dao.CollectedEventDao
import com.victorlapin.usagestats.model.database.entity.CollectedEvent
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers

class CollectedEventsRepository(
    private val mEventDao: CollectedEventDao
) {
    fun insertScreenUnlock() {
        Single.just(
            CollectedEvent(
                eventType = CollectedEvent.TYPE_SCREEN_UNLOCK,
                eventDT = System.currentTimeMillis()
            )
        )
            .map { event -> mEventDao.insert(event) }
            .ignoreElement()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe()
    }

    fun insertNotification(eventDT: Long, packageName: String) {
        Single.just(
            CollectedEvent(
                eventType = CollectedEvent.TYPE_NOTIFICATION,
                eventDT = eventDT,
                packageName = packageName
            )
        )
            .map { event -> mEventDao.insert(event) }
            .ignoreElement()
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe()
    }

    fun clear() {
        Completable.create { emitter ->
            mEventDao.clear()
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe()
    }

    fun getUnlocksCount(dateFrom: Long, dateTo: Long): Int =
        mEventDao.getEventsCount(CollectedEvent.TYPE_SCREEN_UNLOCK, dateFrom, dateTo)

    fun getNotificationsCount(dateFrom: Long, dateTo: Long): Int =
        mEventDao.getEventsCount(CollectedEvent.TYPE_NOTIFICATION, dateFrom, dateTo)

    fun getNotifications(dateFrom: Long, dateTo: Long): Single<List<AggregatedEvent>> =
        mEventDao.getAggregatedEvents(CollectedEvent.TYPE_NOTIFICATION, dateFrom, dateTo)

    fun getUnlocks(dateFrom: Long, dateTo: Long): Single<List<CollectedEvent>> =
        mEventDao.getEvents(CollectedEvent.TYPE_SCREEN_UNLOCK, dateFrom, dateTo)

    fun deleteOldEvents(date: Long): Single<Int> = Single.create<Int> { emitter ->
        emitter.onSuccess(mEventDao.deleteOldEvents(date))
    }

    fun vacuum(): Completable = Completable.create { emitter ->
        mEventDao.vacuum()
        emitter.onComplete()
    }
}