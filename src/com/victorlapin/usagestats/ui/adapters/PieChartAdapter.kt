package com.victorlapin.usagestats.ui.adapters

import androidx.fragment.app.Fragment
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.victorlapin.usagestats.ui.fragments.PieChartFragment

class PieChartAdapter(fm: Fragment, daysToShow: Int) : FragmentStateAdapter(fm) {
    private val mItems = IntProgression
        .fromClosedRange(0, daysToShow * -1, -1).toList()

    override fun getItemCount() = mItems.size

    override fun createFragment(position: Int): Fragment =
        PieChartFragment.newInstance(mItems[position])
}