package com.victorlapin.usagestats.manager

import android.app.NotificationManager
import android.content.Context
import androidx.preference.PreferenceManager
import com.victorlapin.usagestats.R
import java.util.*

class SettingsManager(context: Context) {
    companion object {
        const val KEY_THEME = "interface_theme"
        const val KEY_ABOUT = "open_about"
        const val KEY_CLEAR_DATABASE = "clear_database"
        const val KEY_UNLOCKS_SERVICE = "unlocks_service"
        const val KEY_NOTIFICATIONS_SERVICE = "notifications_service"
        const val KEY_USAGE_SERVICE = "usage_service"
        const val KEY_FIRST_RUN = "first_run"
        const val KEY_RESET_FIRST_RUN = "reset_first_run"
        const val KEY_ENABLE_FILE_LOG = "enable_file_log"
        const val KEY_CANCEL_WORKERS = "cancel_workers"
        const val KEY_DAYS_TO_KEEP = "days_to_keep"
        const val KEY_SHOW_FULL_STATS = "show_full_stats"
        const val KEY_TAP_TARGET = "tap_target"
        const val KEY_WIND_DOWN = "wind_down"
        const val KEY_SYSTEM_NOTIFICATIONS = "system_notifications"
        const val KEY_SYSTEM_DND = "system_dnd"
        const val KEY_WIND_DOWN_ENABLED = "wind_down_enabled"
        const val KEY_WIND_DOWN_START = "wind_down_start"
        const val KEY_WIND_DOWN_END = "wind_down_end"
        const val KEY_WIND_DOWN_GRAYSCALE = "wind_down_grayscale"
        const val KEY_WIND_DOWN_DND = "wind_down_dnd"
        const val KEY_SECURE_SETTINGS = "secure_settings"
        const val KEY_WIND_DOWN_NOW = "wind_down_now"
        const val KEY_WIND_DOWN_DND_MODE = "wind_down_dnd_mode"
        const val KEY_WIND_DOWN_FORCE_STOP = "wind_down_force_stop"
        const val KEY_PHONE_STATE = "phone_state"
        const val KEY_TODAY = "go_to_today"
        const val KEY_SETTINGS = "open_settings"
    }

    private val mPrefs = PreferenceManager.getDefaultSharedPreferences(context)

    var theme: Int
        get() = when (val id =
            mPrefs.getString(KEY_THEME, R.style.AppTheme_Light_Pixel.toString())!!.toInt()) {
            R.style.AppTheme_Light_Pixel,
            R.style.AppTheme_Dark_Pixel,
            R.style.AppTheme_Black_Pixel -> id
            else -> R.style.AppTheme_Light_Pixel
        }
        set(theme) = mPrefs.edit().putString(KEY_THEME, theme.toString()).apply()

    var isFirstRun: Boolean
        get() = mPrefs.getBoolean(KEY_FIRST_RUN, true)
        set(value) = mPrefs.edit().putBoolean(KEY_FIRST_RUN, value).apply()

    var enableFileLog: Boolean
        get() = mPrefs.getBoolean(KEY_ENABLE_FILE_LOG, false)
        set(value) = mPrefs.edit().putBoolean(KEY_ENABLE_FILE_LOG, value).apply()

    var daysToKeep: Int
        get() = mPrefs.getString(KEY_DAYS_TO_KEEP, "30")!!.toInt()
        set(value) = mPrefs.edit().putString(KEY_DAYS_TO_KEEP, value.toString()).apply()

    val showFullStats: Boolean
        get() = mPrefs.getBoolean(KEY_SHOW_FULL_STATS, false)

    var needsTapTarget: Boolean
        get() = mPrefs.getBoolean(KEY_TAP_TARGET, true)
        set(value) = mPrefs.edit().putBoolean(KEY_TAP_TARGET, value).apply()

    val isWindDownEnabled: Boolean
        get() = mPrefs.getBoolean(KEY_WIND_DOWN_ENABLED, false)

    val isWindDownGrayscale: Boolean
        get() = mPrefs.getBoolean(KEY_WIND_DOWN_GRAYSCALE, true)

    val isWindDownDnd: Boolean
        get() = mPrefs.getBoolean(KEY_WIND_DOWN_DND, true)

    var windDownStartTime: Long
        get() = mPrefs.getLong(KEY_WIND_DOWN_START, mDefaultStartTime)
        set(time) = mPrefs.edit().putLong(KEY_WIND_DOWN_START, time).apply()

    var windDownEndTime: Long
        get() = mPrefs.getLong(KEY_WIND_DOWN_END, mDefaultEndTime)
        set(time) = mPrefs.edit().putLong(KEY_WIND_DOWN_END, time).apply()

    private val mDefaultStartTime: Long = Calendar.getInstance(TimeZone.getDefault()).apply {
        set(Calendar.HOUR_OF_DAY, 0)
        set(Calendar.MINUTE, 0)
        set(Calendar.SECOND, 0)
        set(Calendar.MILLISECOND, 0)
    }.timeInMillis

    private val mDefaultEndTime: Long = Calendar.getInstance(TimeZone.getDefault()).apply {
        set(Calendar.HOUR_OF_DAY, 6)
        set(Calendar.MINUTE, 0)
        set(Calendar.SECOND, 0)
        set(Calendar.MILLISECOND, 0)
    }.timeInMillis

    var isWindDownNow: Boolean
        get() = mPrefs.getBoolean(KEY_WIND_DOWN_NOW, false)
        set(isNow) = mPrefs.edit().putBoolean(KEY_WIND_DOWN_NOW, isNow).apply()

    var windDownDndMode: Int
        get() = mPrefs.getInt(
            KEY_WIND_DOWN_DND_MODE,
            NotificationManager.INTERRUPTION_FILTER_ALARMS
        )
        set(mode) = mPrefs.edit().putInt(KEY_WIND_DOWN_DND_MODE, mode).apply()
}