package com.victorlapin.usagestats.work

import android.content.Context
import androidx.work.OneTimeWorkRequest
import androidx.work.RxWorker
import androidx.work.WorkerParameters
import com.victorlapin.usagestats.model.AppsUsage
import com.victorlapin.usagestats.model.DateBuilder
import com.victorlapin.usagestats.model.interactor.AppUsageInteractor
import com.victorlapin.usagestats.model.interactor.NetworkUsageInteractor
import io.reactivex.Completable
import io.reactivex.Single
import org.koin.core.KoinComponent
import org.koin.core.inject
import timber.log.Timber
import java.util.concurrent.TimeUnit

class AppUsageWorker(context: Context, params: WorkerParameters) :
    RxWorker(context, params), KoinComponent {
    private val mInteractor by inject<AppUsageInteractor>()
    private val mNetworkInteractor by inject<NetworkUsageInteractor>()

    override fun createWork(): Single<Result> = mInteractor
        .getStats(DateBuilder(-1))
        .flatMapCompletable { stats ->
            if (stats.source == AppsUsage.SOURCE_SYSTEM && stats.data.isNotEmpty()) {
                mInteractor.insertStats(stats.data)
            } else {
                Completable.complete()
            }
        }
        .andThen(mNetworkInteractor.getStats(DateBuilder(-1)))
        .flatMapCompletable { stats ->
            if (stats.source == AppsUsage.SOURCE_SYSTEM && stats.data.isNotEmpty()) {
                mNetworkInteractor.insertStats(stats.data)
            } else {
                Completable.complete()
            }
        }
        .toSingleDefault(Result.success())
        .onErrorReturnItem(Result.retry())
        .doOnSubscribe {
            Timber.tag(WORK_TAG).i("Worker started")
        }
        .doOnError { Timber.tag(WORK_TAG).e(it) }
        .doFinally {
            Timber.tag(WORK_TAG).i("Worker finished")
        }

    companion object {
        const val WORK_TAG = "AppUsageWorker"

        fun buildRequest(nextRun: Long): OneTimeWorkRequest {
            val dateDiff = nextRun - System.currentTimeMillis()

            return OneTimeWorkRequest.Builder(AppUsageWorker::class.java)
                .setInitialDelay(dateDiff, TimeUnit.MILLISECONDS)
                .build()
        }
    }
}