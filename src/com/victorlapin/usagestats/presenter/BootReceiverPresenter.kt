package com.victorlapin.usagestats.presenter

import com.victorlapin.usagestats.manager.ServicesManager

class BootReceiverPresenter(
    private val mServices: ServicesManager
) {
    fun startScreenUnlockService() =
        mServices.startUnlocksService()
}