package com.victorlapin.usagestats.model.database.entity

import android.graphics.drawable.Drawable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity(tableName = "network_stats")
data class NetworkUsageStats(
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null,
    @ColumnInfo(name = "package_name")
    var packageName: String = "",
    @ColumnInfo(name = "bytes_mobile_t")
    var bytesMobileTx: Long,
    @ColumnInfo(name = "bytes_wifi_t")
    var bytesWifiTx: Long,
    @ColumnInfo(name = "bytes_mobile_r")
    var bytesMobileRx: Long,
    @ColumnInfo(name = "bytes_wifi_r")
    var bytesWifiRx: Long,
    @ColumnInfo(name = "stats_date")
    val statsDate: Long
) {
    @Ignore
    var appName: String? = null
    @Ignore
    var image: Drawable? = null
    @Ignore
    var isDeleted = false
}