package com.victorlapin.usagestats.presenter

import com.victorlapin.usagestats.manager.SettingsManager
import com.victorlapin.usagestats.model.WindDownDateBuilder
import com.victorlapin.usagestats.model.interactor.WindDownInteractor

class WindDownBootReceiverPresenter(
    private val mInteractor: WindDownInteractor,
    private val mSettings: SettingsManager
) {
    fun scheduleNextWindDown() {
        val dateBuilder = WindDownDateBuilder(mSettings.windDownStartTime,
            mSettings.windDownEndTime)
        mInteractor.scheduleWindDown(dateBuilder.nextStartAlarmTime,
            dateBuilder.nextEndAlarmTime).subscribe()
        if (mInteractor.isWindDownNow()) {
            mInteractor.scheduleExitFromPendingWindDown(dateBuilder.previousEndAlarmTime)
        }
    }
}