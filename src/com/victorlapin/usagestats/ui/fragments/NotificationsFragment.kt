package com.victorlapin.usagestats.ui.fragments

import android.os.Bundle
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.model.AggregatedEvent
import com.victorlapin.usagestats.presenter.NotificationsFragmentPresenter
import com.victorlapin.usagestats.ui.adapters.AggregatedAdapter
import com.victorlapin.usagestats.view.NotificationsFragmentView
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.include_toolbar.*
import moxy.ktx.moxyPresenter
import org.koin.androidx.scope.currentScope
import org.koin.core.parameter.parametersOf

class NotificationsFragment : BaseFragment(), NotificationsFragmentView {
    override val layoutRes = R.layout.fragment_list

    private val mDateOffset: Int by lazy {
        arguments!!.getInt(ARG_DATE_OFFSET)
    }

    private val presenter by moxyPresenter {
        currentScope.get<NotificationsFragmentPresenter>(parameters = { parametersOf(mDateOffset) })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbar.setTitle(R.string.action_notifications)
        toolbar.setNavigationIcon(R.drawable.close)
        toolbar.setNavigationOnClickListener { presenter.onBackPressed() }

        list.apply {
            layoutManager = LinearLayoutManager(context)
            itemAnimator = DefaultItemAnimator()
            setHasFixedSize(true)
            adapter = AggregatedAdapter()
        }
    }

    override fun setData(data: List<AggregatedEvent>) {
        list.post { (list.adapter as AggregatedAdapter).setData(data) }
    }

    companion object {
        private const val ARG_DATE_OFFSET = "arg_date_offset"

        fun newInstance(dateOffset: Int): NotificationsFragment {
            val args = Bundle().apply {
                putInt(ARG_DATE_OFFSET, dateOffset)
            }

            return NotificationsFragment().apply {
                arguments = args
            }
        }
    }
}