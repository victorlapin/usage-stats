package com.victorlapin.usagestats.ui.services

import android.service.notification.NotificationListenerService
import android.service.notification.StatusBarNotification
import com.victorlapin.usagestats.presenter.NotificationsServicePresenter
import org.koin.android.ext.android.getKoin
import org.koin.core.qualifier.named

class NotificationsService : NotificationListenerService() {
    private val mScope = getKoin().createScope(
        scopeId = "NotificationsService",
        qualifier = named<NotificationsService>()
    )
    private val mPresenter by mScope.inject<NotificationsServicePresenter>()

    override fun onNotificationPosted(sbn: StatusBarNotification) {
        if (!sbn.isOngoing
            && sbn.isClearable
            && sbn.packageName != "com.android.systemui"
            && sbn.packageName != "android"
        ) {
            mPresenter.insertNotification(sbn.postTime, sbn.packageName)
        }
    }

    override fun onListenerDisconnected() {
        mScope.close()
        super.onListenerDisconnected()
    }
}