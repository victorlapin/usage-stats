package com.victorlapin.usagestats.di

import com.victorlapin.usagestats.presenter.AboutFragmentPresenter
import com.victorlapin.usagestats.presenter.AppsFragmentPresenter
import com.victorlapin.usagestats.presenter.HomeFragmentPresenter
import com.victorlapin.usagestats.presenter.MainActivityPresenter
import com.victorlapin.usagestats.presenter.NetworkFragmentPresenter
import com.victorlapin.usagestats.presenter.NotificationsFragmentPresenter
import com.victorlapin.usagestats.presenter.PieChartFragmentPresenter
import com.victorlapin.usagestats.presenter.UnlocksFragmentPresenter
import com.victorlapin.usagestats.ui.TodayEventBridge
import com.victorlapin.usagestats.ui.activities.MainActivity
import com.victorlapin.usagestats.ui.fragments.AboutFragment
import com.victorlapin.usagestats.ui.fragments.AppsFragment
import com.victorlapin.usagestats.ui.fragments.HomeFragment
import com.victorlapin.usagestats.ui.fragments.NetworkFragment
import com.victorlapin.usagestats.ui.fragments.NotificationsFragment
import com.victorlapin.usagestats.ui.fragments.PieChartFragment
import com.victorlapin.usagestats.ui.fragments.UnlocksFragment
import org.koin.core.qualifier.named
import org.koin.dsl.module

val activitiesModule = module {
    scope(named<MainActivity>()) {
        scoped { MainActivityPresenter(get(), get(), get()) }
    }

    scope(named<HomeFragment>()) {
        scoped {
            HomeFragmentPresenter(get(), get(), get())
        }
    }

    scope(named<AboutFragment>()) {
        scoped { AboutFragmentPresenter(get(), get()) }
    }

    scope(named<NotificationsFragment>()) {
        scoped { params ->
            NotificationsFragmentPresenter(get(), get(), params[0])
        }
    }

    scope(named<UnlocksFragment>()) {
        scoped { params ->
            UnlocksFragmentPresenter(get(), get(), params[0])
        }
    }

    scope(named<AppsFragment>()) {
        scoped { params ->
            AppsFragmentPresenter(get(), get(), params[0])
        }
    }

    scope(named<NetworkFragment>()) {
        scoped { params ->
            NetworkFragmentPresenter(get(), get(), params[0])
        }
    }

    scope(named<PieChartFragment>()) {
        scoped { params ->
            PieChartFragmentPresenter(get(), get(), get(), params[0])
        }
    }

    single { TodayEventBridge() }
}