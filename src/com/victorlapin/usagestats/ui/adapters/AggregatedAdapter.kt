package com.victorlapin.usagestats.ui.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.inflate
import com.victorlapin.usagestats.model.AggregatedEvent
import kotlinx.android.synthetic.main.item_aggregated.view.*

class AggregatedAdapter : RecyclerView.Adapter<AggregatedAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val mItems = ArrayList<AggregatedEvent>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(parent.inflate(R.layout.item_aggregated))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mItems[position]
        holder.itemView.lbl_name.text = item.appName
        holder.itemView.lbl_count.text = item.quantity.toString()
        holder.itemView.image.setImageDrawable(item.image)
    }

    override fun getItemCount() = mItems.size

    fun setData(data: List<AggregatedEvent>) {
        mItems.clear()
        mItems.addAll(data)
        notifyDataSetChanged()
    }
}