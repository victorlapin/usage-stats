package com.victorlapin.usagestats.di

import com.victorlapin.usagestats.presenter.WindDownPresenter
import com.victorlapin.usagestats.presenter.BootReceiverPresenter
import com.victorlapin.usagestats.presenter.WindDownBootReceiverPresenter
import com.victorlapin.usagestats.presenter.WorkerReceiverPresenter
import com.victorlapin.usagestats.ui.receivers.WindDownStartReceiver
import com.victorlapin.usagestats.ui.receivers.BootReceiver
import com.victorlapin.usagestats.ui.receivers.WindDownBootReceiver
import com.victorlapin.usagestats.ui.receivers.WindDownEndReceiver
import com.victorlapin.usagestats.ui.receivers.WorkerReceiver
import org.koin.core.qualifier.named
import org.koin.dsl.module

val receiversModule = module {
    scope(named<BootReceiver>()) {
        scoped { BootReceiverPresenter(get()) }
    }

    scope(named<WorkerReceiver>()) {
        scoped { WorkerReceiverPresenter(get()) }
    }

    scope(named<WindDownStartReceiver>()) {
        scoped { WindDownPresenter(get()) }
    }

    scope(named<WindDownEndReceiver>()) {
        scoped { WindDownPresenter(get()) }
    }

    scope(named<WindDownBootReceiver>()) {
        scoped { WindDownBootReceiverPresenter(get(), get()) }
    }
}