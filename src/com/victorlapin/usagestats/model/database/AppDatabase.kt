package com.victorlapin.usagestats.model.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.victorlapin.usagestats.model.database.dao.AppUsageStatsDao
import com.victorlapin.usagestats.model.database.dao.CollectedEventDao
import com.victorlapin.usagestats.model.database.dao.NetworkUsageStatsDao
import com.victorlapin.usagestats.model.database.entity.AppUsageStats
import com.victorlapin.usagestats.model.database.entity.CollectedEvent
import com.victorlapin.usagestats.model.database.entity.NetworkUsageStats

@Database(
    entities = [
        CollectedEvent::class,
        AppUsageStats::class,
        NetworkUsageStats::class
    ], version = 3, exportSchema = true
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getCollectedEventDao(): CollectedEventDao
    abstract fun getAppUsageStatsDao(): AppUsageStatsDao
    abstract fun getNetworkUsageStatsDao(): NetworkUsageStatsDao

    companion object {
        val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL(
                    "create table app_stats (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            "package_name TEXT NOT NULL, usage INTEGER NOT NULL, " +
                            "launch_count INTEGER NOT NULL, stats_date INTEGER NOT NULL)"
                )
            }
        }

        val MIGRATION_2_3 = object : Migration(2, 3) {
            override fun migrate(db: SupportSQLiteDatabase) {
                db.execSQL(
                    "create table network_stats (id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            "package_name TEXT NOT NULL, bytes_mobile_t INTEGER NOT NULL, " +
                            "bytes_wifi_t INTEGER NOT NULL, bytes_mobile_r INTEGER NOT NULL, " +
                            "bytes_wifi_r INTEGER NOT NULL, stats_date INTEGER NOT NULL)"
                )
            }
        }
    }
}