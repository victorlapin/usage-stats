package com.victorlapin.usagestats.model.interactor

import com.victorlapin.usagestats.model.repository.WindDownRepository
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class WindDownInteractor(
    private val mRepo: WindDownRepository
) {
    fun scheduleWindDown(startTime: Long, endTime: Long): Completable = mRepo.cancelWindDown()
        .andThen(mRepo.scheduleWindDown(startTime, endTime))
        .subscribeOn(Schedulers.computation())
        .observeOn(AndroidSchedulers.mainThread())

    fun cancelWindDown(): Completable = mRepo.cancelWindDown()
        .subscribeOn(Schedulers.computation())
        .observeOn(AndroidSchedulers.mainThread())

    fun turnOn() = mRepo.turnOn()

    fun turnOff() = mRepo.turnOff()

    fun isWindDownNow() = mRepo.isWindDownNow()

    fun scheduleExitFromPendingWindDown(exitTime: Long) =
        mRepo.scheduleExitFromPendingWindDown(exitTime)
}