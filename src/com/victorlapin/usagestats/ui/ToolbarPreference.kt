package com.victorlapin.usagestats.ui

import android.content.Context
import android.util.AttributeSet
import androidx.annotation.StringRes
import androidx.preference.Preference
import androidx.preference.PreferenceViewHolder
import com.victorlapin.usagestats.R
import kotlinx.android.synthetic.main.include_toolbar.view.*
import org.koin.core.KoinComponent
import org.koin.core.inject
import ru.terrakok.cicerone.Router

class ToolbarPreference @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet,
    defStyleAttr: Int = 0
) : Preference(context, attrs, defStyleAttr), KoinComponent {
    @StringRes
    private val mTitleRes = attrs.getAttributeResourceValue(
        "http://schemas.android.com/apk/res/android",
        "title",
        -1
    )
    private val mRouter by inject<Router>()

    init {
        layoutResource = R.layout.include_toolbar
    }

    override fun onBindViewHolder(holder: PreferenceViewHolder) {
        super.onBindViewHolder(holder)
        if (mTitleRes != -1) {
            holder.itemView.toolbar.setTitle(mTitleRes)
        }
        holder.itemView.toolbar.setNavigationIcon(R.drawable.close)
        holder.itemView.toolbar.setNavigationOnClickListener { mRouter.exit() }
        holder.isDividerAllowedBelow = false
    }


}
