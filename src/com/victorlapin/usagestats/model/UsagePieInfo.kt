package com.victorlapin.usagestats.model

import android.content.res.Resources
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import androidx.core.graphics.drawable.DrawableCompat
import androidx.palette.graphics.Palette
import com.razerdp.widget.animatedpieview.data.PieOption
import com.razerdp.widget.animatedpieview.data.SimplePieInfo


class UsagePieInfo(
    value: Double,
    color: Int,
    isDeleted: Boolean,
    val appName: String?,
    val image: Drawable? = null
) : SimplePieInfo() {
    private var mBitmap: Bitmap? = null

    init {
        setColor(color)
        image?.let {
            try {
                mBitmap = when {
                    isDeleted -> {
                        val wrapped = DrawableCompat.wrap(it).apply {
                            setTintMode(PorterDuff.Mode.SRC_IN)
                            setTint(color)
                        }
                        val bitmap =
                            Bitmap.createBitmap(ICON_SIZE_PX, ICON_SIZE_PX, Bitmap.Config.ARGB_8888)
                        val canvas = Canvas(bitmap)
                        wrapped.setBounds(0, 0, ICON_SIZE_PX, ICON_SIZE_PX)
                        wrapped.draw(canvas)
                        bitmap
                    }
                    else -> {
                        val bitmap =
                            Bitmap.createBitmap(ICON_SIZE_PX, ICON_SIZE_PX, Bitmap.Config.ARGB_8888)
                        val canvas = Canvas(bitmap)
                        it.setBounds(0, 0, ICON_SIZE_PX, ICON_SIZE_PX)
                        it.draw(canvas)
                        bitmap
                    }
                }
            } catch (ignore: Exception) {
            }
        }

        setValue(value.toFloat())
        desc = ""
        mBitmap?.let {
            val dominantColor = Palette.from(mBitmap!!).generate().dominantSwatch
            if (dominantColor != null) {
                setColor(dominantColor.rgb)
            }
        }
    }

    private val mOption = PieOption().apply {
        iconHeight = ICON_SIZE_PX.toFloat()
        iconWidth = ICON_SIZE_PX.toFloat()
        iconScaledWidth = 0.5F
        iconScaledHeight = 0.5F
        labelPosition = PieOption.NEAR_PIE
        if (mBitmap != null) {
            labelIcon = mBitmap
        } else {
            super.setDesc(appName ?: "")
        }
    }

    override fun getPieOption(): PieOption? = mOption

    companion object {
        private const val ICON_SIZE_DP = 24
        private val ICON_SIZE_PX =
            (ICON_SIZE_DP * Resources.getSystem().displayMetrics.density).toInt()
    }
}