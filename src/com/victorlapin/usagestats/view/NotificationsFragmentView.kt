package com.victorlapin.usagestats.view

import com.victorlapin.usagestats.model.AggregatedEvent
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.StateStrategyType

interface NotificationsFragmentView : MvpView {
    @StateStrategyType(AddToEndStrategy::class)
    fun setData(data: List<AggregatedEvent>)
}