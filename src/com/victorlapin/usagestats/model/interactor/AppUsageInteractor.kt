package com.victorlapin.usagestats.model.interactor

import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.manager.ResourcesManager
import com.victorlapin.usagestats.manager.ServicesManager
import com.victorlapin.usagestats.manager.SettingsManager
import com.victorlapin.usagestats.model.AppsUsage
import com.victorlapin.usagestats.model.DateBuilder
import com.victorlapin.usagestats.model.SystemUsageStats
import com.victorlapin.usagestats.model.database.entity.AppUsageStats
import com.victorlapin.usagestats.model.repository.AppUsageRepository
import com.victorlapin.usagestats.model.repository.CollectedEventsRepository
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class AppUsageInteractor(
    private val mAppRepo: AppUsageRepository,
    private val mEventsRepo: CollectedEventsRepository,
    private val mResources: ResourcesManager,
    private val mServices: ServicesManager,
    private val mSettings: SettingsManager
) {
    fun getPieStats(db: DateBuilder): Single<SystemUsageStats> =
        mAppRepo.getStats(db.dateFrom, db.dateTo)
            .map {
                if (!mSettings.showFullStats) {
                    (it.data as ArrayList<AppUsageStats>)
                        .retainAll { s -> mServices.packageManager.getLaunchIntentForPackage(s.packageName) != null }
                }
                it
            }
            .map {
                if (it.data.size > 6) {
                    val result = arrayListOf<AppUsageStats>()
                    result.addAll(it.data.take(5))
                    var otherUsage = 0L
                    it.data.drop(5).forEach { stats -> otherUsage += stats.usage }
                    if (otherUsage > 0) {
                        val other = AppUsageStats(
                            packageName = "",
                            usage = otherUsage,
                            statsDate = db.dateFrom
                        )
                        other.appName = mResources.getString(R.string.other)
                        result.add(other)
                    }
                    AppsUsage(it.source, result)
                } else {
                    it
                }
            }
            .map {
                val pm = mServices.packageManager
                it.data.forEach { app ->
                    if (app.packageName.isNotEmpty()) {
                        try {
                            val info = pm.getApplicationInfo(app.packageName, 0)
                            app.image = pm.getApplicationIcon(info)
                        } catch (ignore: Exception) {
                            app.image = mResources.getDrawable(R.drawable.android_head)
                        }
                    }
                }
                it
            }
            .map {
                SystemUsageStats(
                    dateBuilder = db,
                    unlocksCount = mEventsRepo.getUnlocksCount(db.dateFrom, db.dateTo),
                    notificationsCount = mEventsRepo.getNotificationsCount(db.dateFrom, db.dateTo),
                    appsUsage = it
                )
            }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())

    fun getDetailedStats(db: DateBuilder): Single<AppsUsage> =
        mAppRepo.getStats(db.dateFrom, db.dateTo)
            .map {
                if (!mSettings.showFullStats) {
                    (it.data as ArrayList<AppUsageStats>)
                        .retainAll { s -> mServices.packageManager.getLaunchIntentForPackage(s.packageName) != null }
                }
                it
            }
            .map { usage ->
                val pm = mServices.packageManager
                usage.data.forEach {
                    try {
                        val info = pm.getApplicationInfo(it.packageName, 0)
                        it.image = pm.getApplicationIcon(info)
                    } catch (ignore: Exception) {
                    }
                }
                usage
            }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())

    fun getZeroStats(db: DateBuilder): SystemUsageStats =
        SystemUsageStats(
            db, 0, 0,
            AppsUsage(AppsUsage.SOURCE_SYSTEM, listOf())
        )

    fun getStats(db: DateBuilder): Single<AppsUsage> =
        mAppRepo.getStats(db.dateFrom, db.dateTo)
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())

    fun insertStats(data: List<AppUsageStats>): Completable = mAppRepo.insertStats(data)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun clear() = mAppRepo.clear()

    fun deleteOldStats(date: Long): Single<Int> = mAppRepo.deleteOldStats(date)
        .subscribeOn(Schedulers.computation())
        .observeOn(Schedulers.computation())
}