package com.victorlapin.usagestats

object Const {
    const val FRAGMENT_SETTINGS_SERVICES = "fragment_settings_services"
    const val FRAGMENT_SETTINGS_WIND_DOWN = "fragment_settings_wind_down"
}