package com.victorlapin.usagestats.presenter

import com.victorlapin.usagestats.model.DateBuilder
import com.victorlapin.usagestats.model.interactor.NetworkUsageInteractor
import com.victorlapin.usagestats.view.NetworkFragmentView
import io.reactivex.disposables.Disposable
import moxy.InjectViewState
import moxy.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class NetworkFragmentPresenter(
    private val mInteractor: NetworkUsageInteractor,
    private val mRouter: Router,
    private val mDateOffset: Int
) : MvpPresenter<NetworkFragmentView>() {
    override fun attachView(view: NetworkFragmentView?) {
        super.attachView(view)
        getData()
    }

    private fun getData() {
        val dateBuilder = DateBuilder(mDateOffset)
        var disposable: Disposable? = null
        disposable = mInteractor.getDetailedStats(dateBuilder)
            .doOnSubscribe { viewState.toggleProgress(true) }
            .doFinally {
                disposable?.dispose()
                viewState.toggleProgress(false)
            }
            .subscribe({
                viewState.setData(it.data)
            }, {
                it.printStackTrace()
            })
    }

    fun onBackPressed() = mRouter.exit()
}