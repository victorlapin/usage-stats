package com.victorlapin.usagestats.view

import com.victorlapin.usagestats.model.database.entity.AppUsageStats
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.StateStrategyType

interface AppsFragmentView : MvpView {
    @StateStrategyType(AddToEndStrategy::class)
    fun setData(data: List<AppUsageStats>)

    @StateStrategyType(AddToEndStrategy::class)
    fun toggleProgress(isVisible: Boolean)
}