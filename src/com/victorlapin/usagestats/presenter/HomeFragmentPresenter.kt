package com.victorlapin.usagestats.presenter

import com.victorlapin.usagestats.SettingsScreen
import com.victorlapin.usagestats.SettingsServicesScreen
import com.victorlapin.usagestats.di.PROP_DAYS_COUNT_CHANGED
import com.victorlapin.usagestats.manager.ServicesManager
import com.victorlapin.usagestats.manager.SettingsManager
import com.victorlapin.usagestats.view.HomeFragmentView
import moxy.InjectViewState
import moxy.MvpPresenter
import org.koin.core.KoinComponent
import ru.terrakok.cicerone.Router

@InjectViewState
class HomeFragmentPresenter(
    private val mServices: ServicesManager,
    private val mRouter: Router,
    private val mSettings: SettingsManager
) : MvpPresenter<HomeFragmentView>(), KoinComponent {
    private var mLastUsageFlag = true

    override fun onFirstViewAttach() {
        super.onFirstViewAttach()
        mLastUsageFlag = !mServices.isPackageStatsGranted()
        if (mSettings.isFirstRun) {
            viewState.showFirstRun()
        } else if (!mServices.isUnlocksServiceRunning()) {
            viewState.showServiceToast()
        }
    }

    override fun attachView(view: HomeFragmentView?) {
        super.attachView(view)
        val granted = mServices.isPackageStatsGranted()
        if (mLastUsageFlag != granted) {
            mLastUsageFlag = granted
            viewState.invalidatePager(mSettings.daysToKeep)
        }
        if (!mSettings.isFirstRun && mSettings.needsTapTarget) {
            mSettings.needsTapTarget = false
            viewState.showTapTarget()
        }
        if (getKoin().getProperty(PROP_DAYS_COUNT_CHANGED, false)) {
            getKoin().setProperty(PROP_DAYS_COUNT_CHANGED, false)
            viewState.invalidatePager(mSettings.daysToKeep)
        }
    }

    fun openSettings() = mRouter.navigateTo(SettingsScreen())

    fun openServices() = mRouter.navigateTo(SettingsServicesScreen())

    fun checkTapTarget() {
        if (mSettings.needsTapTarget) {
            mSettings.needsTapTarget = false
            viewState.showTapTarget()
        }
    }

    fun disableFirstRun() {
        mSettings.isFirstRun = false
    }
}