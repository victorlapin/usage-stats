package com.victorlapin.usagestats.di

import androidx.room.Room
import com.victorlapin.usagestats.model.database.AppDatabase
import com.victorlapin.usagestats.model.interactor.AboutInteractor
import com.victorlapin.usagestats.model.interactor.AppUsageInteractor
import com.victorlapin.usagestats.model.interactor.CollectedEventsInteractor
import com.victorlapin.usagestats.model.interactor.NetworkUsageInteractor
import com.victorlapin.usagestats.model.interactor.WindDownInteractor
import com.victorlapin.usagestats.model.interactor.WorkerInteractor
import com.victorlapin.usagestats.model.repository.AboutRepository
import com.victorlapin.usagestats.model.repository.AppUsageRepository
import com.victorlapin.usagestats.model.repository.CollectedEventsRepository
import com.victorlapin.usagestats.model.repository.NetworkUsageRepository
import com.victorlapin.usagestats.model.repository.WindDownRepository
import com.victorlapin.usagestats.model.repository.WorkerRepository
import org.koin.dsl.module

val modelModule = module {
    single {
        Room.databaseBuilder(get(), AppDatabase::class.java, "usage_stats.db")
            .addMigrations(
                AppDatabase.MIGRATION_1_2,
                AppDatabase.MIGRATION_2_3
            )
            .build()
    }
    single { get<AppDatabase>().getCollectedEventDao() }
    single { get<AppDatabase>().getAppUsageStatsDao() }
    single { get<AppDatabase>().getNetworkUsageStatsDao() }

    single { AppUsageRepository(get(), get()) }
    single { AppUsageInteractor(get(), get(), get(), get(), get()) }
    single { CollectedEventsRepository(get()) }
    single { CollectedEventsInteractor(get(), get()) }
    single { AboutRepository(get(), get()) }
    single { AboutInteractor(get()) }
    single { WorkerRepository(get()) }
    single { WorkerInteractor(get()) }
    single { WindDownRepository(get(), get()) }
    single { WindDownInteractor(get()) }
    single { NetworkUsageRepository(get(), get()) }
    single { NetworkUsageInteractor(get(), get()) }
}