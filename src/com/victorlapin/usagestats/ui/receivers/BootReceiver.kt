package com.victorlapin.usagestats.ui.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.victorlapin.usagestats.presenter.BootReceiverPresenter
import org.koin.core.KoinComponent
import org.koin.core.qualifier.named

class BootReceiver : BroadcastReceiver(), KoinComponent {
    private var mScope = getKoin().createScope(
        scopeId = "BootReceiver",
        qualifier = named<BootReceiver>()
    )
    private val mPresenter by mScope.inject<BootReceiverPresenter>()

    override fun onReceive(context: Context, intent: Intent) {
        when (intent.action) {
            "android.intent.action.BOOT_COMPLETED",
            "android.intent.action.MY_PACKAGE_REPLACED" ->
                mPresenter.startScreenUnlockService()
        }
        mScope.close()
    }
}