package com.victorlapin.usagestats.di

import com.victorlapin.usagestats.manager.LogManager
import com.victorlapin.usagestats.manager.ResourcesManager
import com.victorlapin.usagestats.manager.ServicesManager
import com.victorlapin.usagestats.manager.SettingsManager
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router

val appModule = module {
    single<Cicerone<Router>> { Cicerone.create() }
    single { get<Cicerone<Router>>().router }
    single { get<Cicerone<Router>>().navigatorHolder }

    single { SettingsManager(androidContext()) }
    single { ResourcesManager(androidContext()) }
    single { ServicesManager(androidContext()) }
    single { LogManager(get()) }
}