package com.victorlapin.usagestats.ui.services

import android.service.quicksettings.Tile
import android.service.quicksettings.TileService
import com.victorlapin.usagestats.manager.ServicesManager
import org.koin.core.KoinComponent
import org.koin.core.inject

class GrayscaleTileService : TileService(), KoinComponent {
    private val mServices by inject<ServicesManager>()

    override fun onStartListening() {
        super.onStartListening()
        val tile = qsTile
        tile.state =
            if (!mServices.isSecureSettingsGranted()) Tile.STATE_UNAVAILABLE
            else if (mServices.isGrayscaled()) Tile.STATE_ACTIVE
            else Tile.STATE_INACTIVE
        tile.updateTile()
    }

    override fun onClick() {
        unlockAndRun {
            val tile = qsTile
            if (mServices.isGrayscaled()) {
                mServices.setGrayscale(false)
                tile.state = Tile.STATE_INACTIVE
            } else {
                mServices.setGrayscale(true)
                tile.state = Tile.STATE_ACTIVE
            }
            tile.updateTile()
        }
    }
}