package com.victorlapin.usagestats.presenter

import com.victorlapin.usagestats.model.DateBuilder
import com.victorlapin.usagestats.model.interactor.CollectedEventsInteractor
import com.victorlapin.usagestats.view.NotificationsFragmentView
import io.reactivex.disposables.Disposable
import moxy.InjectViewState
import moxy.MvpPresenter
import ru.terrakok.cicerone.Router

@InjectViewState
class NotificationsFragmentPresenter(
    private val mInteractor: CollectedEventsInteractor,
    private val mRouter: Router,
    private val mDateOffset: Int
) : MvpPresenter<NotificationsFragmentView>() {
    override fun attachView(view: NotificationsFragmentView?) {
        super.attachView(view)
        getData()
    }

    private fun getData() {
        val dateBuilder = DateBuilder(mDateOffset)
        var disposable: Disposable? = null
        disposable = mInteractor.getNotifications(dateBuilder.dateFrom, dateBuilder.dateTo)
            .doFinally { disposable?.dispose() }
            .subscribe({
                viewState.setData(it)
            }, {
                it.printStackTrace()
            })
    }

    fun onBackPressed() = mRouter.exit()
}