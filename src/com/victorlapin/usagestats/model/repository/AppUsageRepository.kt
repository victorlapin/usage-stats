package com.victorlapin.usagestats.model.repository

import android.app.usage.UsageEvents
import android.content.Intent
import android.content.pm.PackageManager
import com.victorlapin.usagestats.manager.ServicesManager
import com.victorlapin.usagestats.model.AppsUsage
import com.victorlapin.usagestats.model.database.dao.AppUsageStatsDao
import com.victorlapin.usagestats.model.database.entity.AppUsageStats
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

class AppUsageRepository(
    private val mServices: ServicesManager,
    private val mStatsDao: AppUsageStatsDao
) {
    private val mTag = "AppUsageRepository"
    private val mFormatter = SimpleDateFormat
        .getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT)

    private val mLaunchers by lazy {
        val intent = Intent(Intent.ACTION_MAIN)
        intent.addCategory(Intent.CATEGORY_HOME)
        mServices.packageManager.queryIntentActivities(intent, 0)
    }

    fun getStats(dateFrom: Long, dateTo: Long): Single<AppsUsage> =
        mStatsDao.getStats(dateFrom)
            .map { dbstats ->
                Timber.tag(mTag).i(
                    "App usage stats requested for: ${mFormatter.format(dateFrom)} " +
                            "to ${mFormatter.format(dateTo)}"
                )
                Timber.tag(mTag).i("Entities found in the database: ${dbstats.size}")
                if (dbstats.isNotEmpty()) {
                    AppsUsage(AppsUsage.SOURCE_DB, dbstats)
                } else {
                    val result = arrayListOf<AppUsageStats>()
                    val eventsQuery = mServices.usageStatsManager.queryEvents(dateFrom, dateTo)
                    val events = arrayListOf<UsageEvents.Event>()
                    while (eventsQuery.hasNextEvent()) {
                        val event = UsageEvents.Event()
                        eventsQuery.getNextEvent(event)
                        if (event.eventType == UsageEvents.Event.MOVE_TO_FOREGROUND ||
                            event.eventType == UsageEvents.Event.MOVE_TO_BACKGROUND
                        ) {
                            events.add(event)
                        }
                    }

                    val stats = arrayListOf<AppUsageStats>()
                    var usageEntry: AppUsageStats? = null
                    var fgTime = 0L
                    events.forEach {
                        when (it.eventType) {
                            UsageEvents.Event.MOVE_TO_FOREGROUND -> {
                                if (usageEntry != null && it.packageName != usageEntry?.packageName) {
                                    stats.add(usageEntry!!)
                                    usageEntry = null
                                }
                                if (usageEntry == null) {
                                    usageEntry = AppUsageStats(
                                        packageName = it.packageName,
                                        launchCount = 1,
                                        statsDate = dateFrom,
                                        usage = 0L
                                    )
                                }
                                fgTime = it.timeStamp
                            }
                            UsageEvents.Event.MOVE_TO_BACKGROUND -> {
                                usageEntry?.let { entry ->
                                    entry.usage = entry.usage + (it.timeStamp - fgTime)
                                }
                            }
                        }
                    }
                    usageEntry?.let { stats.add(it) }

                    val groupedUsage = mutableMapOf<String, AppUsageStats>()
                    stats.asSequence()
                        .filterNot {
                            it.packageName == mServices.selfPackageName ||
                                    mLaunchers.any { ri -> ri.activityInfo.packageName == it.packageName }
                        }
                        .forEach {
                            if (groupedUsage.containsKey(it.packageName)) {
                                val oldStats = groupedUsage[it.packageName]
                                oldStats!!.usage += it.usage
                                oldStats!!.launchCount += it.launchCount
                            } else {
                                groupedUsage[it.packageName] = it
                            }
                        }

                    groupedUsage.forEach {
                        if (it.value.usage >= TimeUnit.SECONDS.toMillis(1)) {
                            result.add(it.value)
                        }
                    }

                    result.sortByDescending { it.usage }
                    Timber.tag(mTag).i("Entities generated manually: ${result.size}")
                    AppsUsage(AppsUsage.SOURCE_SYSTEM, result)
                }
            }
            .map { usage ->
                usage.data.forEach {
                    it.appName = try {
                        val info = mServices.packageManager.getApplicationInfo(it.packageName, 0)
                        mServices.packageManager.getApplicationLabel(info).toString()
                    } catch (ex: PackageManager.NameNotFoundException) {
                        it.isDeleted = true
                        it.packageName
                    }
                }
                usage
            }

    fun clear() {
        Completable.create { emitter ->
            mStatsDao.clear()
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe()
    }

    fun insertStats(data: List<AppUsageStats>): Completable = Single.just(data)
        .map { stats -> mStatsDao.insert(stats) }
        .ignoreElement()

    fun deleteOldStats(date: Long): Single<Int> = Single.create<Int> { emitter ->
        emitter.onSuccess(mStatsDao.deleteOldStats(date))
    }
}