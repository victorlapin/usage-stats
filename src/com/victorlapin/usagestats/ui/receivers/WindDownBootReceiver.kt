package com.victorlapin.usagestats.ui.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.victorlapin.usagestats.presenter.WindDownBootReceiverPresenter
import org.koin.core.KoinComponent
import org.koin.core.qualifier.named

class WindDownBootReceiver: BroadcastReceiver(), KoinComponent {
    private var mScope = getKoin().createScope(
        scopeId = "WindDownBootReceiver",
        qualifier = named<WindDownBootReceiver>()
    )
    private val mPresenter by mScope.inject<WindDownBootReceiverPresenter>()

    override fun onReceive(context: Context, intent: Intent) {
        when (intent.action) {
            "android.intent.action.BOOT_COMPLETED" ->
                mPresenter.scheduleNextWindDown()
        }
        mScope.close()
    }
}