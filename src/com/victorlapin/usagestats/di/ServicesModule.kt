package com.victorlapin.usagestats.di

import com.victorlapin.usagestats.presenter.NotificationsServicePresenter
import com.victorlapin.usagestats.presenter.ScreenUnlockServicePresenter
import com.victorlapin.usagestats.ui.services.NotificationsService
import com.victorlapin.usagestats.ui.services.ScreenUnlockService
import org.koin.core.qualifier.named
import org.koin.dsl.module

val servicesModule = module {
    scope(named<ScreenUnlockService>()) {
        scoped { ScreenUnlockServicePresenter(get()) }
    }

    scope(named<NotificationsService>()) {
        scoped { NotificationsServicePresenter(get()) }
    }
}