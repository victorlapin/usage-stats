package com.victorlapin.usagestats.model

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

class DateBuilder(val offset: Int = 0) {
    val dateFrom: Long
    val dateTo: Long

    init {
        val cal = Calendar.getInstance()
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        cal.add(Calendar.DAY_OF_MONTH, offset)
        dateFrom = cal.timeInMillis
        cal.add(Calendar.DAY_OF_MONTH, 1)
        cal.add(Calendar.SECOND, -1)
        dateTo = cal.timeInMillis
    }

    val formattedDate: String = SimpleDateFormat
        .getDateInstance(DateFormat.MEDIUM)
        .format(dateFrom)
}
