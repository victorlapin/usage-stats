package com.victorlapin.usagestats.model

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class WindDownDateBuilder constructor() {
    private val mMidnight = Calendar.getInstance(TimeZone.getDefault()).apply {
        set(Calendar.HOUR_OF_DAY, 0)
        set(Calendar.MINUTE, 0)
        set(Calendar.SECOND, 0)
        set(Calendar.MILLISECOND, 0)
    }.timeInMillis

    private val mStartCalendar = Calendar.getInstance(TimeZone.getDefault()).apply {
        timeInMillis = mMidnight
    }
    private val mEndCalendar = Calendar.getInstance(TimeZone.getDefault()).apply {
        timeInMillis = mMidnight
    }

    constructor(startDate: Long, endDate: Long) : this() {
        val cal = Calendar.getInstance(TimeZone.getDefault())
        cal.timeInMillis = startDate

        mStartCalendar.apply {
            set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY))
            set(Calendar.MINUTE, cal.get(Calendar.MINUTE))
        }
        cal.timeInMillis = endDate
        mEndCalendar.apply {
            set(Calendar.HOUR_OF_DAY, cal.get(Calendar.HOUR_OF_DAY))
            set(Calendar.MINUTE, cal.get(Calendar.MINUTE))
        }

        if (mEndCalendar.timeInMillis < mStartCalendar.timeInMillis) {
            mEndCalendar.add(Calendar.DAY_OF_YEAR, 1)
        }
    }

    val nextStartAlarmTime: Long
        get() {
            while (mStartCalendar.timeInMillis < System.currentTimeMillis()) {
                mStartCalendar.add(Calendar.DAY_OF_YEAR, 1)
            }
            return mStartCalendar.timeInMillis
        }

    val nextEndAlarmTime: Long
        get() {
            while (mEndCalendar.timeInMillis < nextStartAlarmTime) {
                mEndCalendar.add(Calendar.DAY_OF_YEAR, 1)
            }
            while (mEndCalendar.timeInMillis < System.currentTimeMillis()) {
                mEndCalendar.add(Calendar.DAY_OF_YEAR, 1)
            }
            return mEndCalendar.timeInMillis
        }

    val previousStartAlarmTime: Long
        get() = nextStartAlarmTime - ONE_DAY

    val previousEndAlarmTime: Long
        get() = nextEndAlarmTime - ONE_DAY

    fun isInRangeNow(): Boolean {
        val current = System.currentTimeMillis()
        var result =
            current.coerceIn(mStartCalendar.timeInMillis, mEndCalendar.timeInMillis) == current
        if (!result) {
            result = current.coerceIn(
                mStartCalendar.timeInMillis - ONE_DAY,
                mEndCalendar.timeInMillis - ONE_DAY
            ) == current
        }

        return result
    }

    companion object {
        val timeFormatter: DateFormat = SimpleDateFormat.getTimeInstance(DateFormat.SHORT)
        private val ONE_DAY = TimeUnit.DAYS.toMillis(1)
    }
}
