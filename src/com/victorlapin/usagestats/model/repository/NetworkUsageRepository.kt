package com.victorlapin.usagestats.model.repository

import android.app.usage.NetworkStats
import android.content.pm.PackageManager
import android.net.ConnectivityManager
import android.os.Process
import com.victorlapin.usagestats.manager.ServicesManager
import com.victorlapin.usagestats.model.NetworkUsage
import com.victorlapin.usagestats.model.database.dao.NetworkUsageStatsDao
import com.victorlapin.usagestats.model.database.entity.NetworkUsageStats
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.schedulers.Schedulers
import timber.log.Timber
import java.text.DateFormat
import java.text.SimpleDateFormat

class NetworkUsageRepository(
    private val mServices: ServicesManager,
    private val mStatsDao: NetworkUsageStatsDao
) {
    private val mTag = "NetworkUsageRepository"
    private val mFormatter = SimpleDateFormat
        .getDateTimeInstance(DateFormat.SHORT, DateFormat.SHORT)

    fun getStats(dateFrom: Long, dateTo: Long): Single<NetworkUsage> =
        mStatsDao.getStats(dateFrom)
            .map { dbstats ->
                Timber.tag(mTag).i(
                    "Network usage stats requested for: ${mFormatter.format(dateFrom)} " +
                            "to ${mFormatter.format(dateTo)}"
                )
                Timber.tag(mTag).i("Entities found in the database: ${dbstats.size}")
                if (dbstats.isNotEmpty()) {
                    NetworkUsage(NetworkUsage.SOURCE_DB, dbstats)
                } else {
                    val groupedUsage = mutableMapOf<Int, NetworkUsageStats>()
                    val bucket = NetworkStats.Bucket()
                    mServices.networkStatsManager.queryDetails(
                        ConnectivityManager.TYPE_WIFI,
                        "",
                        dateFrom,
                        dateTo
                    ).use {
                        while (it.hasNextBucket()) {
                            if (it.getNextBucket(bucket) && Process.isApplicationUid(bucket.uid)) {
                                if (groupedUsage.containsKey(bucket.uid)) {
                                    val entry = groupedUsage[bucket.uid]
                                    entry!!.bytesWifiTx += bucket.txBytes
                                    entry!!.bytesWifiRx += bucket.rxBytes
                                } else {
                                    val entry = NetworkUsageStats(
                                        bytesMobileTx = 0L,
                                        bytesMobileRx = 0L,
                                        bytesWifiTx = bucket.txBytes,
                                        bytesWifiRx = bucket.rxBytes,
                                        statsDate = dateFrom
                                    )
                                    groupedUsage[bucket.uid] = entry
                                }
                            }
                        }
                    }

                    mServices.networkStatsManager.queryDetails(
                        ConnectivityManager.TYPE_MOBILE,
                        mServices.getSubscriberId(),
                        dateFrom,
                        dateTo
                    ).use {
                        while (it.hasNextBucket()) {
                            if (it.getNextBucket(bucket) && Process.isApplicationUid(bucket.uid)) {
                                if (groupedUsage.containsKey(bucket.uid)) {
                                    val entry = groupedUsage[bucket.uid]
                                    entry!!.bytesMobileTx += bucket.txBytes
                                    entry!!.bytesMobileRx += bucket.rxBytes
                                } else {
                                    val entry = NetworkUsageStats(
                                        bytesMobileTx = bucket.txBytes,
                                        bytesMobileRx = bucket.rxBytes,
                                        bytesWifiTx = 0L,
                                        bytesWifiRx = 0L,
                                        statsDate = dateFrom
                                    )
                                    groupedUsage[bucket.uid] = entry
                                }
                            }
                        }
                    }

                    groupedUsage.forEach { (uid, stats) ->
                        val packages = mServices.packageManager.getPackagesForUid(uid)
                        packages?.let {
                            if (it.size == 1) {
                                stats.packageName = it.first()
                            }
                        }
                    }

                    val result = groupedUsage.values
                        .filter { it.packageName.isNotEmpty() }
                        .toMutableList()
                    result.sortByDescending {
                        it.bytesMobileTx + it.bytesWifiTx + it.bytesMobileRx + it.bytesWifiRx
                    }
                    Timber.tag(mTag).i("Entities generated manually: ${result.size}")
                    NetworkUsage(NetworkUsage.SOURCE_SYSTEM, result)
                }
            }
            .map { usage ->
                usage.data.forEach {
                    it.appName = try {
                        val info = mServices.packageManager.getApplicationInfo(it.packageName, 0)
                        mServices.packageManager.getApplicationLabel(info).toString()
                    } catch (ex: PackageManager.NameNotFoundException) {
                        it.isDeleted = true
                        it.packageName
                    }
                }
                usage
            }

    fun clear() {
        Completable.create { emitter ->
            mStatsDao.clear()
            emitter.onComplete()
        }
            .subscribeOn(Schedulers.io())
            .observeOn(Schedulers.io())
            .subscribe()
    }

    fun insertStats(data: List<NetworkUsageStats>): Completable = Single.just(data)
        .map { stats -> mStatsDao.insert(stats) }
        .ignoreElement()

    fun deleteOldStats(date: Long): Single<Int> = Single.create { emitter ->
        emitter.onSuccess(mStatsDao.deleteOldStats(date))
    }

    fun getTotalBytes(dateFrom: Long, dateTo: Long): Single<Long> = Single.create { emitter ->
        var result = mStatsDao.getTotalBytes(dateFrom)
        if (result == 0L) {
            val stats = getStats(dateFrom, dateTo).blockingGet()
            if (stats.source == NetworkUsage.SOURCE_SYSTEM) {
                result += stats.data
                    .sumByDouble { it.bytesMobileRx.toDouble() + it.bytesWifiRx }
                    .toLong()
            }
        }

        emitter.onSuccess(result)
    }
}