package com.victorlapin.usagestats

import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import androidx.room.Room
import androidx.room.testing.MigrationTestHelper
import androidx.sqlite.db.SupportSQLiteDatabase
import androidx.sqlite.db.framework.FrameworkSQLiteOpenHelperFactory
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import com.victorlapin.usagestats.model.DateBuilder
import com.victorlapin.usagestats.model.database.AppDatabase
import org.junit.Assert.assertEquals
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class DatabaseMigrationTest {
    @Rule
    @JvmField
    val mTestHelper = MigrationTestHelper(
        InstrumentationRegistry.getInstrumentation(),
        AppDatabase::class.java.canonicalName,
        FrameworkSQLiteOpenHelperFactory()
    )

    @Test
    fun migrate_12() {
        val db = mTestHelper.createDatabase(TEST_DB_NAME, 1)

        mTestHelper.runMigrationsAndValidate(
            TEST_DB_NAME, 2,
            false, AppDatabase.MIGRATION_1_2
        )
        val dateBuilder = DateBuilder()
        insertStats(
            1L, "com.victorlapin.test", 0L,
            1, dateBuilder.dateFrom, db
        )
        val dao = getMigratedDatabase().getAppUsageStatsDao()

        val result = dao.getStats(dateBuilder.dateFrom).blockingGet()
        assertEquals(1, result.size)
        val entry = result[0]
        assertEquals("com.victorlapin.test", entry.packageName)
    }

    @Test
    fun migrate_23() {
        val db = mTestHelper.createDatabase(TEST_DB_NAME, 2)

        mTestHelper.runMigrationsAndValidate(
            TEST_DB_NAME, 3,
            false, AppDatabase.MIGRATION_2_3
        )
        val dateBuilder = DateBuilder()
        insertNetworkStats(
            1L, "com.victorlapin.test", 0L,
            1024L, dateBuilder.dateFrom, db
        )
        val dao = getMigratedDatabase().getNetworkUsageStatsDao()

        val result = dao.getStats(dateBuilder.dateFrom).blockingGet()
        assertEquals(1, result.size)
        val entry = result[0]
        assertEquals("com.victorlapin.test", entry.packageName)
        assertEquals(1024L, entry.bytesWifiTx)
        assertEquals(0L, entry.bytesMobileRx)
    }

    private fun insertStats(
        id: Long, packageName: String, usage: Long,
        launchCount: Int, statsDate: Long,
        db: SupportSQLiteDatabase
    ) {
        val cv = ContentValues()
        cv.put("id", id)
        cv.put("package_name", packageName)
        cv.put("usage", usage)
        cv.put("launch_count", launchCount)
        cv.put("stats_date", statsDate)
        db.insert("app_stats", SQLiteDatabase.CONFLICT_REPLACE, cv)
    }

    private fun insertNetworkStats(
        id: Long, packageName: String, bytesMobile: Long,
        bytesWifi: Long, statsDate: Long,
        db: SupportSQLiteDatabase
    ) {
        val cv = ContentValues()
        cv.put("id", id)
        cv.put("package_name", packageName)
        cv.put("bytes_mobile_t", bytesMobile)
        cv.put("bytes_wifi_t", bytesWifi)
        cv.put("bytes_mobile_r", bytesMobile)
        cv.put("bytes_wifi_r", bytesWifi)
        cv.put("stats_date", statsDate)
        db.insert("network_stats", SQLiteDatabase.CONFLICT_REPLACE, cv)
    }

    private fun getMigratedDatabase(): AppDatabase {
        val database = Room.databaseBuilder(
            InstrumentationRegistry.getInstrumentation().targetContext,
            AppDatabase::class.java,
            TEST_DB_NAME
        )
            .allowMainThreadQueries()
            .addMigrations(
                AppDatabase.MIGRATION_1_2,
                AppDatabase.MIGRATION_2_3
            )
            .build()
        mTestHelper.closeWhenFinished(database)
        return database
    }

    companion object {
        const val TEST_DB_NAME = "usagestats_test.db"
    }
}