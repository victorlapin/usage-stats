package com.victorlapin.usagestats.model.interactor

import com.victorlapin.usagestats.manager.ServicesManager
import com.victorlapin.usagestats.model.DateBuilder
import com.victorlapin.usagestats.model.NetworkUsage
import com.victorlapin.usagestats.model.database.entity.NetworkUsageStats
import com.victorlapin.usagestats.model.repository.NetworkUsageRepository
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class NetworkUsageInteractor(
    private val mNetworkRepo: NetworkUsageRepository,
    private val mServices: ServicesManager
) {
    fun getDetailedStats(db: DateBuilder): Single<NetworkUsage> =
        mNetworkRepo.getStats(db.dateFrom, db.dateTo)
            .map { usage ->
                val pm = mServices.packageManager
                usage.data.forEach {
                    try {
                        val info = pm.getApplicationInfo(it.packageName, 0)
                        it.image = pm.getApplicationIcon(info)
                    } catch (ignore: Exception) {
                    }
                }
                usage
            }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())

    fun getStats(db: DateBuilder): Single<NetworkUsage> =
        mNetworkRepo.getStats(db.dateFrom, db.dateTo)
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())

    fun insertStats(data: List<NetworkUsageStats>): Completable = mNetworkRepo.insertStats(data)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())

    fun clear() = mNetworkRepo.clear()

    fun deleteOldStats(date: Long): Single<Int> = mNetworkRepo.deleteOldStats(date)
        .subscribeOn(Schedulers.computation())
        .observeOn(Schedulers.computation())

    fun getTotalBytes(db: DateBuilder): Single<Long> =
        mNetworkRepo.getTotalBytes(db.dateFrom, db.dateTo)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
}