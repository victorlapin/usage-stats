package com.victorlapin.usagestats.ui.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import com.victorlapin.usagestats.presenter.WorkerReceiverPresenter
import org.koin.core.KoinComponent
import org.koin.core.qualifier.named

class WorkerReceiver : BroadcastReceiver(), KoinComponent {
    private var mScope = getKoin().createScope(
        scopeId = "WorkerReceiver",
        qualifier = named<WorkerReceiver>()
    )
    private val mPresenter by mScope.inject<WorkerReceiverPresenter>()

    override fun onReceive(context: Context, intent: Intent) {
        when (intent.action) {
            "android.intent.action.BOOT_COMPLETED" ->
                mPresenter.scheduleAppUsageWorker()
        }
        mScope.close()
    }
}