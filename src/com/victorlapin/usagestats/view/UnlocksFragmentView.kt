package com.victorlapin.usagestats.view

import com.victorlapin.usagestats.model.database.entity.CollectedEvent
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.StateStrategyType

interface UnlocksFragmentView : MvpView {
    @StateStrategyType(AddToEndStrategy::class)
    fun setData(data: List<CollectedEvent>, dateFrom: Long, dateTo: Long)
}