package com.victorlapin.usagestats.ui.adapters

import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.inflate
import com.victorlapin.usagestats.manager.ResourcesManager
import com.victorlapin.usagestats.model.database.entity.AppUsageStats
import kotlinx.android.synthetic.main.item_app.view.*
import java.util.concurrent.TimeUnit

class AppsAdapter(
    resources: ResourcesManager,
    private val clickListener: (String) -> Unit = {}
) : RecyclerView.Adapter<AppsAdapter.ViewHolder>() {
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)

    private val mItems = ArrayList<AppUsageStats>()

    private val mLaunchCountStr = resources.getString(R.string.app_opened)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        ViewHolder(parent.inflate(R.layout.item_app))

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mItems[position]
        holder.itemView.container.setOnClickListener { clickListener(item.packageName) }
        holder.itemView.container.alpha = if (item.isDeleted) 0.5f else 1f
        holder.itemView.lbl_name.text = item.appName
        holder.itemView.lbl_package.text = item.packageName
        holder.itemView.image.setImageDrawable(
            item.image
                ?: holder.itemView.context.getDrawable(R.drawable.android_head)
        )
        holder.itemView.lbl_launch_count.text = mLaunchCountStr.format(item.launchCount)
        holder.itemView.lbl_time.text = formatDuration(item.usage)
    }

    private fun formatDuration(duration: Long): String {
        return "%01d:%02d:%02d".format(
            TimeUnit.MILLISECONDS.toHours(duration),
            TimeUnit.MILLISECONDS.toMinutes(duration) % TimeUnit.HOURS.toMinutes(1),
            TimeUnit.MILLISECONDS.toSeconds(duration) % TimeUnit.MINUTES.toSeconds(1)
        )
    }

    override fun getItemCount() = mItems.size

    fun setData(data: List<AppUsageStats>) {
        mItems.clear()
        mItems.addAll(data)
        notifyDataSetChanged()
    }
}