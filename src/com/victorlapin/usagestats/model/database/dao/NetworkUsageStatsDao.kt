package com.victorlapin.usagestats.model.database.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Transaction
import com.victorlapin.usagestats.model.database.entity.NetworkUsageStats
import io.reactivex.Single

@Dao
interface NetworkUsageStatsDao {
    @Query("select * from network_stats where stats_date = :dateFrom")
    fun getStats(dateFrom: Long): Single<List<NetworkUsageStats>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(stats: List<NetworkUsageStats>)

    @Transaction
    @Query("delete from network_stats")
    fun clear()

    @Transaction
    @Query("delete from network_stats where stats_date < :date")
    fun deleteOldStats(date: Long): Int

    @Query("select sum(bytes_mobile_r) + sum(bytes_wifi_r) from network_stats where stats_date = :dateFrom")
    fun getTotalBytes(dateFrom: Long): Long
}