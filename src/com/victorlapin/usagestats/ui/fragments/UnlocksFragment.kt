package com.victorlapin.usagestats.ui.fragments

import android.os.Bundle
import android.util.TypedValue
import com.jjoe64.graphview.DefaultLabelFormatter
import com.jjoe64.graphview.GridLabelRenderer
import com.jjoe64.graphview.helper.StaticLabelsFormatter
import com.jjoe64.graphview.series.BarGraphSeries
import com.jjoe64.graphview.series.DataPoint
import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.model.database.entity.CollectedEvent
import com.victorlapin.usagestats.presenter.UnlocksFragmentPresenter
import com.victorlapin.usagestats.view.UnlocksFragmentView
import kotlinx.android.synthetic.main.fragment_unlocks.*
import kotlinx.android.synthetic.main.include_toolbar.*
import moxy.ktx.moxyPresenter
import org.koin.androidx.scope.currentScope
import org.koin.core.parameter.parametersOf
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

class UnlocksFragment : BaseFragment(), UnlocksFragmentView {
    override val layoutRes = R.layout.fragment_unlocks

    private val mDateOffset: Int by lazy {
        arguments!!.getInt(ARG_DATE_OFFSET)
    }

    private val presenter by moxyPresenter {
        currentScope.get<UnlocksFragmentPresenter>(parameters = { parametersOf(mDateOffset) })
    }

    private val mAccentColor by lazy {
        val typedValue = TypedValue()
        val theme = context!!.theme
        theme.resolveAttribute(R.attr.colorAccent, typedValue, true)
        typedValue.data
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        toolbar.setTitle(R.string.action_unlocks)
        toolbar.setNavigationIcon(R.drawable.close)
        toolbar.setNavigationOnClickListener { presenter.onBackPressed() }

        val formatter = StaticLabelsFormatter(chart, DefaultLabelFormatter())
        val labels = arrayListOf<String>()
        val sdf = SimpleDateFormat.getTimeInstance(DateFormat.SHORT)
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)
        for (i in 0..24) {
            labels.add(sdf.format(calendar.timeInMillis))
            calendar.add(Calendar.HOUR_OF_DAY, 1)
        }
        formatter.setHorizontalLabels(labels.toTypedArray())

        chart.apply {
            viewport.apply {
                isXAxisBoundsManual = true
                setMinX(0.0)
                setMaxX(24.0)
                isYAxisBoundsManual = true
                setMinY(0.0)
                isScrollable = true
            }
            gridLabelRenderer.apply {
                gridStyle = GridLabelRenderer.GridStyle.BOTH
                setHorizontalLabelsAngle(90)
                labelsSpace = 20
                labelFormatter = formatter
            }
        }
    }

    override fun setData(data: List<CollectedEvent>, dateFrom: Long, dateTo: Long) {
        chart.removeAllSeries()
        var timeLeft = dateFrom
        var timeRight = timeLeft + TimeUnit.HOURS.toMillis(1) - 1
        val timeEnd = data.last().eventDT
        val entries = arrayListOf<DataPoint>()
        var x = 0
        while (timeLeft <= timeEnd) {
            val count = data.count { it.eventDT in timeLeft..timeRight }
            if (count > 0) {
                entries.add(DataPoint(x.toDouble() + 0.5, count.toDouble()))
            }
            timeLeft = timeRight
            timeRight = timeLeft + TimeUnit.HOURS.toMillis(1) - 1
            x++
        }

        val series = BarGraphSeries<DataPoint>(entries.toTypedArray())
        series.apply {
            spacing = if (entries.size == 1) 100 else 50
            color = mAccentColor
            isDrawValuesOnTop = true
            valuesOnTopColor = mAccentColor
            valuesOnTopSize = 50F
            isAnimated = true
        }

        chart.viewport.setMaxY(series.highestValueY + 2.0)
        chart.addSeries(series)
    }

    companion object {
        private const val ARG_DATE_OFFSET = "arg_date_offset"

        fun newInstance(dateOffset: Int): UnlocksFragment {
            val args = Bundle().apply {
                putInt(ARG_DATE_OFFSET, dateOffset)
            }

            return UnlocksFragment().apply {
                arguments = args
            }
        }
    }
}