package com.victorlapin.usagestats.view

import com.victorlapin.usagestats.model.database.entity.NetworkUsageStats
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.StateStrategyType

interface NetworkFragmentView : MvpView {
    @StateStrategyType(AddToEndStrategy::class)
    fun setData(data: List<NetworkUsageStats>)

    @StateStrategyType(AddToEndStrategy::class)
    fun toggleProgress(isVisible: Boolean)
}