package com.victorlapin.usagestats.presenter

import com.victorlapin.usagestats.AppsScreen
import com.victorlapin.usagestats.NetworkScreen
import com.victorlapin.usagestats.NotificationsScreen
import com.victorlapin.usagestats.UnlocksScreen
import com.victorlapin.usagestats.model.AppsUsage
import com.victorlapin.usagestats.model.DateBuilder
import com.victorlapin.usagestats.model.interactor.AppUsageInteractor
import com.victorlapin.usagestats.model.interactor.NetworkUsageInteractor
import com.victorlapin.usagestats.view.PieChartFragmentView
import io.reactivex.disposables.Disposable
import moxy.InjectViewState
import moxy.MvpPresenter
import ru.terrakok.cicerone.Router
import timber.log.Timber

@InjectViewState
class PieChartFragmentPresenter(
    private val mAppUsageInteractor: AppUsageInteractor,
    private val mNetworkUsageInteractor: NetworkUsageInteractor,
    private val mRouter: Router,
    private var mDateOffset: Int
) : MvpPresenter<PieChartFragmentView>() {

    override fun attachView(view: PieChartFragmentView?) {
        super.attachView(view)
        getData()
    }

    fun getData() {
        val dateBuilder = DateBuilder(mDateOffset)
        var disposable: Disposable? = null
        disposable = mAppUsageInteractor.getPieStats(dateBuilder)
            .doOnSubscribe {
                viewState.toggleProgress(true)
                viewState.showManualSaveButton(false)
                viewState.setData(mAppUsageInteractor.getZeroStats(dateBuilder))
            }
            .doFinally { disposable?.dispose() }
            .subscribe({
                viewState.toggleProgress(false)
                viewState.setData(it)
                viewState.showManualSaveButton(
                    mDateOffset < 0 &&
                            it.appsUsage.source != AppsUsage.SOURCE_DB &&
                            it.appsUsage.data.isNotEmpty()
                )
            }, {
                viewState.toggleProgress(false)
                Timber.e(it)
            })

        var networkDisposable: Disposable? = null
        networkDisposable = mNetworkUsageInteractor.getTotalBytes(dateBuilder)
            .doOnSubscribe {
                viewState.toggleNetworkProgress(true)
                viewState.setNetworkData(0L)
            }
            .doFinally {
                viewState.toggleNetworkProgress(false)
                networkDisposable?.dispose()
            }
            .subscribe({
                viewState.setNetworkData(it)
            }, {
                Timber.e(it)
            })
    }

    fun openNotifications() = mRouter.navigateTo(NotificationsScreen(mDateOffset))

    fun openUnlocks() = mRouter.navigateTo(UnlocksScreen(mDateOffset))

    fun openApps() = mRouter.navigateTo(AppsScreen(mDateOffset))

    fun openTraffic() = mRouter.navigateTo(NetworkScreen(mDateOffset))

    fun manualSaveStats() {
        val dateBuilder = DateBuilder(mDateOffset)
        var disposable: Disposable? = null
        disposable = mAppUsageInteractor.getStats(dateBuilder)
            .flatMapCompletable { mAppUsageInteractor.insertStats(it.data) }
            .andThen(mNetworkUsageInteractor.getStats(dateBuilder))
            .flatMapCompletable { mNetworkUsageInteractor.insertStats(it.data) }
            .doOnSubscribe {
                viewState.enableManualSaveButton(false)
                viewState.toggleProgress(true)
            }
            .doOnTerminate {
                viewState.enableManualSaveButton(true)
                viewState.toggleProgress(false)
            }
            .doFinally { disposable?.dispose() }
            .subscribe { viewState.showManualSaveButton(false) }
    }
}