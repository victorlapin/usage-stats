package com.victorlapin.usagestats.ui.fragments

import android.os.Bundle
import android.widget.Toast
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.SwitchPreference
import com.afollestad.assent.Permission
import com.afollestad.assent.askForPermissions
import com.afollestad.materialdialogs.MaterialDialog
import com.afollestad.materialdialogs.lifecycle.lifecycleOwner
import com.afollestad.materialdialogs.list.listItemsSingleChoice
import com.victorlapin.usagestats.NotificationAccessScreen
import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.UsageAccessScreen
import com.victorlapin.usagestats.manager.ResourcesManager
import com.victorlapin.usagestats.manager.ServicesManager
import com.victorlapin.usagestats.manager.SettingsManager
import io.reactivex.Completable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router

class SettingsServicesFragment : PreferenceFragmentCompat() {
    private val mResources by inject<ResourcesManager>()
    private val mRouter by inject<Router>()
    private val mServices by inject<ServicesManager>()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences_services)
    }

    override fun onResume() {
        super.onResume()

        val unlocksPreference =
            findPreference<SwitchPreference>(SettingsManager.KEY_UNLOCKS_SERVICE)
        unlocksPreference!!.isChecked = mServices.isUnlocksServiceRunning()
        unlocksPreference.setOnPreferenceChangeListener { _, newValue ->
            val isEnabled = newValue as Boolean
            if (isEnabled) {
                mServices.startUnlocksService()
                mServices.enableBootReceiver()
            } else {
                mServices.stopUnlocksService()
                mServices.disableBootReceiver()
            }
            return@setOnPreferenceChangeListener true
        }

        val notificationsPreference =
            findPreference<Preference>(SettingsManager.KEY_NOTIFICATIONS_SERVICE)
        notificationsPreference!!.summary = mResources.getString(
            if (mServices.isNotificationsAccessGranted())
                R.string.pref_notifications_service_summary_on else
                R.string.pref_notifications_service_summary_off
        )
        notificationsPreference.setOnPreferenceClickListener {
            mRouter.navigateTo(NotificationAccessScreen())
            return@setOnPreferenceClickListener true
        }

        val usagePreference = findPreference<Preference>(SettingsManager.KEY_USAGE_SERVICE)
        usagePreference!!.summary = mResources.getString(
            if (mServices.isPackageStatsGranted())
                R.string.pref_usage_service_summary_on else
                R.string.pref_usage_service_summary_off
        )
        usagePreference.setOnPreferenceClickListener {
            mRouter.navigateTo(UsageAccessScreen())
            return@setOnPreferenceClickListener true
        }

        val securePreference = findPreference<Preference>(SettingsManager.KEY_SECURE_SETTINGS)
        val isSecureSettingsGranted = mServices.isSecureSettingsGranted()
        securePreference!!.summary = mResources.getString(
            if (isSecureSettingsGranted)
                R.string.pref_secure_settings_summary_on else
                R.string.pref_secure_settings_summary_off
        )
        if (!isSecureSettingsGranted) {
            securePreference.setOnPreferenceClickListener {
                val adbDialog = MaterialDialog(context!!).apply {
                    lifecycleOwner(this@SettingsServicesFragment)
                    title(res = R.string.secure_settings_adb)
                    message(
                        text = context.getString(
                            R.string.secure_settings_adb_text,
                            mServices.adbCommand
                        )
                    )
                    positiveButton(res = android.R.string.ok)
                }

                MaterialDialog(context!!).show {
                    lifecycleOwner(this@SettingsServicesFragment)
                    title(res = R.string.pref_secure_settings_title)
                    listItemsSingleChoice(res = R.array.secure_settings_variants) { dialog, index, _ ->
                        when (index) {
                            0 -> {
                                it.summary = context.getString(R.string.wait)
                                Completable.create { emitter ->
                                    Runtime.getRuntime().exec(mServices.suCommand).waitFor()
                                    emitter.onComplete()
                                }
                                    .subscribeOn(Schedulers.computation())
                                    .observeOn(AndroidSchedulers.mainThread())
                                    .subscribe({
                                        it.summary =
                                            context.getString(R.string.pref_secure_settings_summary_on)
                                        it.onPreferenceClickListener = null
                                    }, {
                                        securePreference.summary =
                                            context.getString(R.string.pref_secure_settings_summary_off)
                                        Toast.makeText(context, it.message, Toast.LENGTH_SHORT)
                                            .show()
                                    })
                            }
                            1 -> {
                                dialog.dismiss()
                                adbDialog.show()
                            }
                        }
                    }
                    negativeButton(res = android.R.string.cancel)
                    positiveButton(res = android.R.string.ok)
                }
                return@setOnPreferenceClickListener true
            }
        }

        val phonePreference =
            findPreference<Preference>(SettingsManager.KEY_PHONE_STATE)
        phonePreference!!.summary = mResources.getString(
            if (mServices.isPhoneStateGranted())
                R.string.pref_phone_state_summary_on else
                R.string.pref_phone_state_summary_off
        )
        phonePreference.setOnPreferenceClickListener {
            if (!mServices.isPhoneStateGranted()) {
                askForPermissions(Permission.READ_PHONE_STATE) {
                    phonePreference.summary = mResources.getString(
                        if (it.isAllGranted(Permission.READ_PHONE_STATE))
                            R.string.pref_phone_state_summary_on else
                            R.string.pref_phone_state_summary_off
                    )
                }
            }
            return@setOnPreferenceClickListener true
        }
    }

    companion object {
        fun newInstance() = SettingsServicesFragment()
    }
}