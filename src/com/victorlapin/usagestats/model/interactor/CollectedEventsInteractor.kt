package com.victorlapin.usagestats.model.interactor

import android.content.pm.PackageManager
import com.victorlapin.usagestats.manager.ServicesManager
import com.victorlapin.usagestats.model.AggregatedEvent
import com.victorlapin.usagestats.model.database.entity.CollectedEvent
import com.victorlapin.usagestats.model.repository.CollectedEventsRepository
import io.reactivex.Completable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class CollectedEventsInteractor(
    private val mRepo: CollectedEventsRepository,
    private val mServices: ServicesManager
) {
    fun insertScreenUnlock() = mRepo.insertScreenUnlock()

    fun insertNotification(eventDT: Long, packageName: String) =
        mRepo.insertNotification(eventDT, packageName)

    fun clear() = mRepo.clear()

    fun getNotifications(dateFrom: Long, dateTo: Long): Single<List<AggregatedEvent>> =
        mRepo.getNotifications(dateFrom, dateTo)
            .map { events ->
                events.forEach {
                    try {
                        val info = mServices.packageManager.getApplicationInfo(it.appName, 0)
                        it.appName = mServices.packageManager.getApplicationLabel(info).toString()
                        it.image = mServices.packageManager.getApplicationIcon(info)
                    } catch (ignore: PackageManager.NameNotFoundException) {
                    }
                }
                events
            }
            .subscribeOn(Schedulers.computation())
            .observeOn(AndroidSchedulers.mainThread())

    fun getUnlocks(dateFrom: Long, dateTo: Long): Single<List<CollectedEvent>> =
        mRepo.getUnlocks(dateFrom, dateTo)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())

    fun deleteOldEvents(date: Long): Single<Int> = mRepo.deleteOldEvents(date)
        .subscribeOn(Schedulers.computation())
        .observeOn(Schedulers.computation())

    fun vacuum(): Completable = mRepo.vacuum()
}