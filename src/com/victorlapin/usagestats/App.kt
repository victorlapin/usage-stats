package com.victorlapin.usagestats

import android.app.Application
import com.victorlapin.usagestats.di.PROP_DAYS_COUNT_CHANGED
import com.victorlapin.usagestats.di.PROP_STATS_MODE_CHANGED
import com.victorlapin.usagestats.di.allModules
import com.victorlapin.usagestats.manager.LogManager
import com.victorlapin.usagestats.manager.ServicesManager
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.android.logger.AndroidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.EmptyLogger
import org.koin.core.logger.Level

class App : Application() {
    private val mServices by inject<ServicesManager>()
    private val mLogs by inject<LogManager>()

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@App)
            logger(if (BuildConfig.DEBUG) AndroidLogger(Level.DEBUG) else EmptyLogger())
            modules(allModules)
            properties(
                mapOf(
                    PROP_STATS_MODE_CHANGED to false,
                    PROP_DAYS_COUNT_CHANGED to false
                )
            )
        }
        mServices.createNotificationChannels()
        mLogs.onStartup()
    }
}