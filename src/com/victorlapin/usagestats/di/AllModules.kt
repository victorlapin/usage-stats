package com.victorlapin.usagestats.di

val allModules = listOf(
    appModule,
    modelModule,
    activitiesModule,
    servicesModule,
    receiversModule
)

const val PROP_STATS_MODE_CHANGED = "stats_mode_changed"
const val PROP_DAYS_COUNT_CHANGED = "days_count_changed"