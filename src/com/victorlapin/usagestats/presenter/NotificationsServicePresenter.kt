package com.victorlapin.usagestats.presenter

import com.victorlapin.usagestats.model.interactor.CollectedEventsInteractor

class NotificationsServicePresenter(
    private val mEventsInteractor: CollectedEventsInteractor
) {
    fun insertNotification(eventDT: Long, packageName: String) =
        mEventsInteractor.insertNotification(eventDT, packageName)
}