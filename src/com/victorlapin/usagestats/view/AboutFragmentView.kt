package com.victorlapin.usagestats.view

import com.victorlapin.usagestats.model.repository.AboutRepository
import moxy.MvpView
import moxy.viewstate.strategy.AddToEndStrategy
import moxy.viewstate.strategy.StateStrategyType

interface AboutFragmentView : MvpView {
    @StateStrategyType(AddToEndStrategy::class)
    fun setData(data: List<AboutRepository.ListItem>)
}