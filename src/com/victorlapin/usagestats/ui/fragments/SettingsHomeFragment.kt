package com.victorlapin.usagestats.ui.fragments

import android.os.Build
import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import com.victorlapin.usagestats.R
import com.victorlapin.usagestats.SettingsScreen
import com.victorlapin.usagestats.SettingsWindDownScreen
import com.victorlapin.usagestats.SystemDndScreen
import com.victorlapin.usagestats.SystemNotificationsScreen
import com.victorlapin.usagestats.manager.ServicesManager
import com.victorlapin.usagestats.manager.SettingsManager
import com.victorlapin.usagestats.model.WindDownDateBuilder
import com.victorlapin.usagestats.ui.TodayEventBridge
import org.koin.android.ext.android.inject
import ru.terrakok.cicerone.Router
import java.util.*

class SettingsHomeFragment : PreferenceFragmentCompat() {
    private val mRouter by inject<Router>()
    private val mSettings by inject<SettingsManager>()
    private val mServices by inject<ServicesManager>()
    private val mTodayBridge by inject<TodayEventBridge>()

    override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
        addPreferencesFromResource(R.xml.preferences_home)
        val pm = context!!.packageManager

        val dndPreference = findPreference<Preference>(SettingsManager.KEY_SYSTEM_DND)!!
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val dndResolved =
                pm.resolveActivity(SystemDndScreen().getActivityIntent(context), 0) != null
            if (dndResolved) {
                dndPreference.setOnPreferenceClickListener {
                    mRouter.navigateTo(SystemDndScreen())
                    return@setOnPreferenceClickListener true
                }
            } else {
                dndPreference.isEnabled = false
                dndPreference.summary = context!!.getString(R.string.unavailable)
            }
        } else {
            dndPreference.isEnabled = false
            dndPreference.summary = context!!.getString(R.string.unavailable)
        }

        val windDownPreference = findPreference<Preference>(SettingsManager.KEY_WIND_DOWN)!!
        windDownPreference.setOnPreferenceClickListener {
            mRouter.navigateTo(SettingsWindDownScreen())
            return@setOnPreferenceClickListener true
        }

        val notifPreference = findPreference<Preference>(SettingsManager.KEY_SYSTEM_NOTIFICATIONS)!!
        val notifResolved =
            pm.resolveActivity(SystemNotificationsScreen().getActivityIntent(context), 0) != null
        if (notifResolved) {
            notifPreference.setOnPreferenceClickListener {
                mRouter.navigateTo(SystemNotificationsScreen())
                return@setOnPreferenceClickListener true
            }
        } else {
            notifPreference.isEnabled = false
            notifPreference.summary = context!!.getString(R.string.unavailable)
        }

        val settingsGlobalPref = findPreference<Preference>(SettingsManager.KEY_SETTINGS)!!
        settingsGlobalPref.setOnPreferenceClickListener {
            mRouter.navigateTo(SettingsScreen())
            return@setOnPreferenceClickListener true
        }

        val todayPref = findPreference<Preference>(SettingsManager.KEY_TODAY)!!
        todayPref.setOnPreferenceClickListener {
            if (mTodayBridge.todayClickedEvent.hasObservers()) {
                mTodayBridge.todayClickedEvent.onNext(Any())
            }
            return@setOnPreferenceClickListener true
        }
    }

    override fun onResume() {
        super.onResume()

        val windDownPreference = findPreference<Preference>(SettingsManager.KEY_WIND_DOWN)!!
        if (mSettings.isWindDownEnabled) {
            val s = arrayListOf<String>()
            if (mSettings.isWindDownGrayscale && mServices.isSecureSettingsGranted()) {
                s.add(context!!.getString(R.string.pref_wind_down_grayscale_title))
            }
            if (mSettings.isWindDownDnd && mServices.isNotificationPolicyAccessGranted()) {
                s.add(context!!.getString(R.string.pref_system_dnd_title))
            }
            if (s.isEmpty()) {
                s.add(context!!.getString(R.string.pref_wind_down_summary_off))
            } else {
                s.add(
                    context!!.getString(
                        R.string.dnd_from_to,
                        WindDownDateBuilder.timeFormatter.format(Date(mSettings.windDownStartTime)),
                        WindDownDateBuilder.timeFormatter.format(Date(mSettings.windDownEndTime))
                    )
                )
            }
            windDownPreference.summary = s.joinToString()
        } else {
            windDownPreference.summary = context!!.getString(R.string.off)
        }
    }
}